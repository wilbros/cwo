<?php

namespace Chef\AdminBundle\Service\Uploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface Uploader
 */
interface Uploader {
    /**
     * Get upload path
     *
     * @return string
     */
    public function getUploadPath();

    /**
     * Upload file
     *
     * @param UploadedFile $file       File to be uploaded
     * @param string       $filename   (Optional) Filename after file has been uploaded. Default to current filename.
     *
     * @return string Uploaded file public path
     */
    public function upload(UploadedFile $file, $filename = "");

	/**
	 * @param $folder
	 * @return $this
	 */
	public function setFolder($folder);

    /**
     * Remove current file from specific location
     *
     * @param string    $filename Name of the file to be removed
     */
    public function remove($filename);
}