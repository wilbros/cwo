<?php

namespace Chef\AdminBundle\Controller;

use Chef\AdminBundle\Service\Uploader\Uploader;
use Chef\DomainBundle\Entity\ChefRecipe\ChefRecipe;
use Chef\DomainBundle\Entity\ChefRecipe\ChefSliderTrx;
use Chef\DomainBundle\Entity\ChefRecipe\DinnerMenu;
use Chef\DomainBundle\Entity\ChefRecipe\Ingredient;
use Chef\DomainBundle\Entity\ChefRecipe\Preparation;
use Chef\DomainBundle\Entity\ChefRecipe\Presentation;
use Chef\DomainBundle\Entity\ChefRecipe\RecipeMenu;
use Chef\DomainBundle\Entity\ChefRecipe\Step;
use Chef\DomainBundle\Entity\ChefRecipe\Tag;
use Chef\DomainBundle\Entity\HealthyRecipe\DinnerMenuHealthy;
use Chef\DomainBundle\Entity\HealthyRecipe\HealthyRecipe;
use Chef\DomainBundle\Entity\HealthyRecipe\HealthySlider;
use Chef\DomainBundle\Entity\HealthyRecipe\HealthyTag;
use Chef\DomainBundle\Entity\HealthyRecipe\RecipeMenuHealthy;
use Chef\DomainBundle\Entity\Slider;
use Chef\DomainBundle\Repository\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class RecipesController extends BaseController {
	private $repo, $em, $uploader, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "uploader" = @JMS\Inject("uploader.local")
	 * })
	 */
	public function __construct(Repository $repo, Uploader $uploader, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->uploader = $uploader;

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

	/**
	 * @Route("/chef-recipes/add")
	 * @Method({"GET"})
	 */
	public function chefAddAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
		$dinnerMenus = $repoDinner->findAll();

		$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
		$recipeMenus = $repoRecipe->findAll();

		return $this->render('chef/add.html.twig', [
			'dinnerMenus' => $dinnerMenus,
			'recipeMenus' => $recipeMenus
		]);
	}

	/**
	 * @Route("/chef-recipes/add")
	 * @Method({"POST"})
	 */
	public function chefPostAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$data = $request->request;
		$files = $request->files;

		$uploader = $this->uploader->setFolder('ChefRecipes');

		if($data->has('id')) {
			$repo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');

			/** @var $recipe ChefRecipe */
			$recipe = $repo->find($data->get('id'));

			$recipe->setTitle($data->get('title'));

			$recipe->setFirstParagraph($data->get('firstParagraph'));

			$recipe->setSecondParagraph($data->get('secondParagraph'));

			/** @var $presentation Presentation */
			$presentation = $recipe->getPresentation();

			if($files->get('presentation')['image'] != null) {
				$presentationImagePath = $uploader->upload($files->get('presentation')['image']);

				$presentation->setFeaturedImage($presentationImagePath);
			}

			$presentation->setTips($data->get('presentation')['chefTips']);

			if($files->get('image') != null) {
				$featuredImagePath = $uploader->upload($files->get('image'));

				$recipe->setFeaturedImage($featuredImagePath);
			}

			$presentation->clearStep($this->em);
			foreach($data->get('presentation')['step'] as $step) {
				$presentation->addStep(new Step($presentation, $step));
			}

			$recipe->setPresentation($presentation);

			$tags = explode(',', $data->get('tags'));

			$recipe->clearTags($this->em);
			foreach($tags as $tag) {
				$tagRepo = $this->repo->getRepositoryOf('ChefRecipe\Tag');
				$tagTemp = $tagRepo->findOneBy(['name' => $tag]);

				if(!$tagTemp) {
					$tag = new Tag($tag);
				} else {
					$tag = $tagTemp;
				}

				$recipe->addTag($tag);
			}

			$repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
			$recipe->clearDinnerMenu($this->em);
			foreach($data->get('dinnerMenu') as $dinnerMenu) {
				$dinner = $repoDinner->find(intval($dinnerMenu));
				$recipe->addDinnerMenu($dinner);
			}

			$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
			$recipe->clearRecipeMenu($this->em);
			foreach($data->get('recipeMenu') as $recipeMenu) {
				$recipeM = $repoRecipe->find(intval($recipeMenu));
				$recipe->addRecipeMenu($recipeM);
			}

			if($data->has('preparation')) {
				//add preparation
				$recipe->clearPreparations($this->em);
				foreach($data->get('preparation') as $preparation) {
					$preparationTx = new Preparation($recipe, $preparation['title'], $preparation['method']);

					$preparationTx->clearIngredient($this->em);
					foreach($preparation['ingredients'] as $ingredient) {
						$preparationTx->addIngredient(
							new Ingredient($preparationTx, $ingredient)
						);
					}

					$recipe->addPreparation($preparationTx);
				}
			}

		} else {
			$presentationImagePath = $uploader->upload($files->get('presentation')['image']);

			$featuredImagePath = $uploader->upload($files->get('image'));

			$presentation = new Presentation($data->get('presentation')['title'], $data->get('presentation')['chefTips'], $presentationImagePath);

			foreach($data->get('presentation')['step'] as $step) {
				$presentation->addStep(new Step($presentation, $step));
			}

			$recipe = new ChefRecipe(
				$data->get('title'),
				$data->get('firstParagraph'),
				$data->get('secondParagraph'),
				$presentation,
				$featuredImagePath
			);

			$tags = explode(',', $data->get('tags'));
			foreach($tags as $tag) {
				$tagRepo = $this->repo->getRepositoryOf('ChefRecipe\Tag');
				$tagTemp = $tagRepo->findOneBy(['name' => $tag]);

				if(!$tagTemp) {
					$tag = new Tag($tag);
				} else {
					$tag = $tagTemp;
				}

				$recipe->addTag($tag);
			}

			$repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
			foreach($data->get('dinnerMenu') as $dinnerMenu) {
				$dinner = $repoDinner->find(intval($dinnerMenu));
				$recipe->addDinnerMenu($dinner);
			}

			$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
			foreach($data->get('recipeMenu') as $recipeMenu) {
				$recipeM = $repoRecipe->find(intval($recipeMenu));
				$recipe->addRecipeMenu($recipeM);
			}
		}

		if($data->has('isSlider')) {
			$slider = $this->findSliderBelongToChef($recipe);

			if($slider === false) {
				$slider = new ChefSliderTrx($recipe);

				$recipe->setSlider($slider);

				$this->em->persist($slider);
			}
		} else {
			$slider = $this->findSliderBelongToChef($recipe);

			if($slider !== false) {
				$recipe->removeSlider();

				$this->em->remove($slider);
			}
		}

		$this->em->persist($recipe);
		$this->em->flush();

		if($data->has('id')) {
			$id = $data->get('id');

			return $this->redirectWithFlash("/admin/chef-recipes/edit/{$id}", $this->session, [
				'message' => [
					'code' => 200,
					'type' => 'update'
				]
			]);
		}

		return $this->redirectWithFlash("/admin/chef-recipes/edit/{$recipe->getId()}", $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'insert'
			]
		]);
	}

	/**
	 * @Route("/chef-recipes/delete/{id}")
	 * @Method({"GET"})
	 */
	public function chefDeleteAction(Request $request, $id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');

		/** @var $data ChefRecipe */
		$data = $repo->find($id);

		if($data->getSlider() != null) {
			$this->em->remove($data->getSlider());
		}

		$this->em->remove($data);
		$this->em->flush();

		return $this->redirectWithFlash('/admin/chef-recipes', $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'delete'
			]
		]);
	}

	/**
	 * @Route("/chef-recipes/edit/{id}")
	 * @Method({"GET"})
	 */
	public function chefEditAction($id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$recipeRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		$recipe = $recipeRepo->find($id);

		$repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
		$dinnerMenus = $repoDinner->findAll();

		$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
		$recipeMenus = $repoRecipe->findAll();

		return $this->render('chef/add.html.twig', [
			'dinnerMenus' => $dinnerMenus,
			'recipeMenus' => $recipeMenus,
			'recipetx' => $recipe
		]);
	}

	/**
	 * @Route("/chef-recipes/edit/{id}/new/preparation")
	 * @Method({"GET"})
	 */
	public function addPreparationAction($id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
		$dinnerMenus = $repoDinner->findAll();

		$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
		$recipeMenus = $repoRecipe->findAll();

		$recipeRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		$recipe = $recipeRepo->find($id);

		return $this->render('chef/add-preparation.html.twig', [
			'dinnerMenus' => $dinnerMenus,
			'recipeMenus' => $recipeMenus,
			'recipetx' => $recipe
		]);
	}

	/**
	 * @Route("/chef-recipes/edit/{id}/new/preparation")
	 * @Method({"POST"})
	 */
	public function addPostPreparationAction($id, Request $request) {
		$data = $request->request;

		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$recipeRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		$recipe = $recipeRepo->find($id);

		//add preparation
		foreach($data->get('preparation') as $preparation) {
			$preparationTx = new Preparation($recipe, $preparation['title'], $preparation['method']);

			foreach($preparation['ingredients'] as $ingredient) {
				$preparationTx->addIngredient(
					new Ingredient($preparationTx, $ingredient)
				);
			}

			$recipe->addPreparation($preparationTx);
		}

		$this->em->persist($recipe);
		$this->em->flush();

		return $this->redirectWithFlash("/admin/chef-recipes/edit/{$recipe->getId()}", $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'insert'
			]
		]);
	}

	/**
	 * @Route("/chef-recipes/edit/{id}/edit/preparation/{pid}")
	 * @Method({"GET"})
	 */
	public function editGetPreparationAction($id, $pid, Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$recipeRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		/** @var $recipe ChefRecipe */
		$recipe = $recipeRepo->find($id);

		$preparation = $recipe->getPreparations()->filter(function(Preparation $preparation) use($pid) {
			return $preparation->getId() == $pid;
		});

		return $this->render('chef/add-preparation.html.twig', [
			'recipetx' => $recipe,
			'preparation' => $preparation->first()
		]);
	}

	/**
	 * @Route("/chef-recipes/edit/{id}/edit/preparation/{pid}")
	 * @Method({"POST"})
	 */
	public function editPostPreparationAction($id, $pid, Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$data = $request->request;

		$recipeRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		/** @var $recipe ChefRecipe */
		$recipe = $recipeRepo->find($id);

		$preparation = $recipe->getPreparations()->filter(function(Preparation $preparation) use($pid) {
			return $preparation->getId() == $pid;
		});

		if(!$preparation) {
			throw new \Exception('Preparation Id not found!');
		}

		/** @var $preparation Preparation */
		$preparation = $preparation->first();

		//add preparation
		foreach($data->get('preparation') as $preparationTx) {
			$preparation->setPreparation($preparationTx['title']);
			$preparation->setMethod($preparationTx['method']);

			$preparation->clearIngredient($this->em);
			foreach($preparationTx['ingredients'] as $ingredient) {
				$preparation->addIngredient(
					new Ingredient($preparation, $ingredient)
				);
			}

			$recipe->addPreparation($preparation);
		}

		$this->em->persist($recipe);
		$this->em->flush();

		return $this->redirectWithFlash($request->getRequestUri(), $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'update'
			]
		]);
	}

	/**
	 * @Route("/chef-recipes/edit/{id}/delete/preparation/{pid}")
	 * @Method({"GET"})
	 */
	public function deletePostPreparationAction($id, $pid, Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$recipeRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		/** @var $recipe ChefRecipe */
		$recipe = $recipeRepo->find($id);

		$preparation = $recipe->getPreparations()->filter(function(Preparation $preparation) use($pid) {
			return $preparation->getId() == $pid;
		});

		if(!$preparation) {
			throw new \Exception('Preparation Id not found!');
		}

		/** @var $preparation Preparation */
		$preparation = $preparation->first();

		if($recipe->removePreparation($preparation, $this->em)) {
			$this->em->persist($recipe);
			$this->em->flush();
		}

		return $this->redirectWithFlash("/admin/chef-recipes/edit/{$recipe->getId()}", $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'update'
			]
		]);

	}

	public function findSliderBelongToChef(ChefRecipe $feed) {
		$repo = $this->repo->getRepositoryOf('Slider');
		$sliders = $repo->findAll();

		$collection = new ArrayCollection($sliders);
		$slider = $collection->filter(function(Slider $slider) use($feed) {
			return ($slider instanceof ChefSliderTrx)
			and ($slider->getReference()->getId() === $feed->getId());
		})->first();

		return $slider;
	}

	/**
	 * @Route("/chef-recipes")
	 * @Method({"GET"})
	 */
	public function chefAllAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		$recipes = $repoRecipe->findAll();

		return $this->render('chef/all.html.twig', [
			'recipes' => $recipes
		]);
	}

	/**
	 * @Route("/healthy-recipes")
	 * @Method({"GET"})
	 */
	public function healthyAllAction() {
        if(!$this->session->has(SESSION_KEY)) {
            return new RedirectResponse('/admin/login');
        }

        $repo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');

        $data = $repo->findAll();

        return $this->render('healthy/all.html.twig', [
            'recipes' => $data
        ]);
	}

	/**
	 * @Route("/healthy-recipes/add")
	 * @Method({"GET"})
	 */
	public function healthyAddAction() {
        if(!$this->session->has(SESSION_KEY)) {
            return new RedirectResponse('/admin/login');
        }

        $repoDinner = $this->repo->getRepositoryOf('HealthyRecipe\DinnerMenuHealthy');
        $dinnerMenus = $repoDinner->findAll();

        $repoRecipe = $this->repo->getRepositoryOf('HealthyRecipe\RecipeMenuHealthy');
        $recipeMenus = $repoRecipe->findAll();

        return $this->render('healthy/add.html.twig', [
            'dinnerMenus' => $dinnerMenus,
            'recipeMenus' => $recipeMenus
        ]);
	}

	/**
	 * @Route("/healthy-recipes/add")
	 * @Method({"POST"})
	 */
	public function healthyPostAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$data = $request->request;

		$uploader = $path = $this->uploader->setFolder('healthy');

		if($data->has('id')) {
			$repo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');

			/** @var $feed HealthyRecipe */
			$feed = $repo->find($data->get('id'));

			if($request->files->get('image') !== null) {
				$path = $uploader->upload($request->files->get('image'));
				$feed->setFeaturedImage($path);
			}

			$feed->setTitle($data->get('title'));
			$feed->setContent($data->get('content'));

            $tags = explode(',', $data->get('tags'));

            $feed->clearTags($this->em);
            foreach($tags as $tag) {
                $tagRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyTag');
                $tagTemp = $tagRepo->findOneBy(['name' => $tag]);

                if(!$tagTemp) {
                    $tag = new HealthyTag($tag);
                } else {
                    $tag = $tagTemp;
                }

                $feed->addTag($tag);
            }

		} else {
			$path = $uploader->upload($request->files->get('image'));
			$feed = new HealthyRecipe($data->get('title'), $data->get('content'), $path);

            $tags = explode(',', $data->get('tags'));
            foreach($tags as $tag) {
                $tagRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyTag');
                $tagTemp = $tagRepo->findOneBy(['name' => $tag]);

                if(!$tagTemp) {
                    $tag = new HealthyTag($tag);
                } else {
                    $tag = $tagTemp;
                }

                $feed->addTag($tag);
            }
		}

        $repoDinner = $this->repo->getRepositoryOf('HealthyRecipe\DinnerMenuHealthy');
        $feed->clearDinnerMenu($this->em);
        foreach($data->get('dinnerMenu') as $dinnerMenu) {
            $dinner = $repoDinner->find(intval($dinnerMenu));
            $feed->addDinnerMenu($dinner);
        }

        $repoRecipe = $this->repo->getRepositoryOf('HealthyRecipe\RecipeMenuHealthy');
        $feed->clearRecipeMenu($this->em);
        foreach($data->get('recipeMenu') as $recipeMenu) {
            $recipeM = $repoRecipe->find(intval($recipeMenu));
            $feed->addRecipeMenu($recipeM);
        }

		$this->em->persist($feed);

		if($data->has('isSlider')) {
			$slider = $this->findSliderBelongTo($feed);

			if($slider === false) {
				$slider = new HealthySlider($feed);
				$this->em->persist($slider);
			}
		} else {
			$slider = $this->findSliderBelongTo($feed);

			if($slider !== false) {
				$feed->removeSlider();

				$this->em->remove($slider);
			}
		}

		$this->em->flush();

		if($data->has('id')) {
			$id = $data->get('id');

			return $this->redirectWithFlash("/admin/healthy-recipes/edit/{$id}", $this->session, [
				'message' => [
					'code' => 200,
					'type' => 'update'
				]
			]);
		}

		return $this->redirectWithFlash($request->getRequestUri(), $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'insert'
			]
		]);
	}

	public function findSliderBelongTo(HealthyRecipe $feed) {
		$repo = $this->repo->getRepositoryOf('Slider');
		$sliders = $repo->findAll();

		$collection = new ArrayCollection($sliders);
		$slider = $collection->filter(function(Slider $slider) use($feed) {
			return ($slider instanceof HealthySlider)
			and ($slider->getReference()->getId() === $feed->getId());
		})->first();

		return $slider;
	}

	/**
	 * @Route("/healthy-recipes/edit/{id}")
	 * @Method({"GET"})
	 */
	public function healthyEditAction(Request $request, $id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');

		$data = $repo->find($id);

        $repoDinner = $this->repo->getRepositoryOf('HealthyRecipe\DinnerMenuHealthy');
        $dinnerMenus = $repoDinner->findAll();

        $repoRecipe = $this->repo->getRepositoryOf('HealthyRecipe\RecipeMenuHealthy');
        $recipeMenus = $repoRecipe->findAll();

		return $this->render('healthy/add.html.twig', [
			'healthy' => $data,
            'dinnerMenus' => $dinnerMenus,
            'recipeMenus' => $recipeMenus
		]);
	}

	/**
	 * @Route("/healthy-recipes/delete/{id}")
	 * @Method({"GET"})
	 */
	public function healthyDeleteAction(Request $request, $id) {

		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		};

		$repo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');

		/** @var $data healthyRecipe */
		$data = $repo->find($id);

		if($data->getSlider() != null) {
			$this->em->remove($data->getSlider());
		}

		$this->em->remove($data);
		$this->em->flush();

		return $this->redirectWithFlash('/admin/healthy-recipes', $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'delete'
			]
		]);
	}
}
