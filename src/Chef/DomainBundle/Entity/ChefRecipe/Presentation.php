<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Presentation
 * @package Chef\DomainBundle\Entity\ChefRecipe
 * @ORM\Entity
 */
class Presentation extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $title;

	/** @ORM\OneToMany(targetEntity="Step", mappedBy="presentation", cascade={"all"}) */
	private $steps;

	/** @ORM\Column(type="text") */
	private $tips;

	/** @ORM\Column(type="text") */
	private $featuredImage;

	public function __construct($title, $tips, $featuredImage) {
		parent::__construct();
		$this->title = $title;
		$this->steps = new ArrayCollection();
		$this->tips = $tips;
		$this->featuredImage = $featuredImage;
	}

	/**
	 * @return mixed
	 */
	public function getSteps()
	{
		return $this->steps;
	}

	/**
	 * @param mixed $steps
	 */
	public function setSteps($steps)
	{
		$this->steps = $steps;
	}

	public function clearStep(EntityManagerInterface $em) {
		foreach($this->steps as $step) {
			$em->remove($step);
		}
		$this->steps->clear();

		$em->flush();

		return $this;
	}

	/**
	 * @param mixed $steps
	 */
	public function addStep(Step $step)
	{
		$this->steps->add($step);

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTips()
	{
		return $this->tips;
	}

	/**
	 * @param mixed $tips
	 */
	public function setTips($tips)
	{
		$this->tips = $tips;
	}

	/**
	 * @return mixed
	 */
	public function getFeaturedImage()
	{
		return $this->featuredImage;
	}

	/**
	 * @param mixed $featuredImage
	 */
	public function setFeaturedImage($featuredImage)
	{
		$this->featuredImage = $featuredImage;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
} 