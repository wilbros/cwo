<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\DomainBundle\Entity\Slider;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ChefSliderTrx extends Slider {

	/** @ORM\OneToOne(targetEntity="ChefRecipe") */
	private $reference;

	public function __construct(ChefRecipe $chefRecipe) {
		parent::__construct();

		$this->reference = $chefRecipe;
	}

	public function getReference() {
		return $this->reference;
	}

	function getName() {
		return "Chef Recipe";
	}

	function getAdminBackLink() {
		return sprintf("/admin/%s/edit/%s", "chef-recipes", $this->reference->getId());
	}

	function getBackLink() {
		return sprintf("/%s/%s", "recipes", $this->reference->getSlug());
	}
}