<?php

namespace Chef\DomainBundle\Entity\HealthyRecipe;

use Chef\DomainBundle\Entity\Slider;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class HealthySlider extends Slider {

	/** @ORM\OneToOne(targetEntity="HealthyRecipe") */
	private $reference;

	public function __construct(HealthyRecipe $healthyRecipe) {
		parent::__construct();

		$this->reference = $healthyRecipe;

		$healthyRecipe->setSlider($this);
	}

	public function getReference() {
		return $this->reference;
	}

	public function getName() {
		return 'Healthy Recipe';
	}

	function getAdminBackLink() {
		return sprintf("/admin/%s/edit/%s", "healthy-recipes", $this->reference->getId());
	}

	function getBackLink() {
		return sprintf("/%s/%s", "healthy", $this->reference->getSlug());
	}
}