<?php

namespace Chef\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ChefAdminBundle extends Bundle {
	public function __construct() {
		if(!defined('BUNDLE_SEPARATOR')) define('BUNDLE_SEPARATOR', ':');

		define('SESSION_KEY', 'CWO_SESSION_LOG');
	}
} 