<?php

namespace Chef\AdminBundle\Controller;

use Chef\AdminBundle\Service\Uploader\Uploader;
use Chef\DomainBundle\Entity\Slider;
use Chef\DomainBundle\Entity\Workout\FeedSlider;
use Chef\DomainBundle\Entity\Workout\WorkOutFeed;
use Chef\DomainBundle\Repository\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class WorkoutController extends BaseController {
	private $repo, $em, $uploader, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "uploader" = @JMS\Inject("uploader.local")
	 * })
	 */
	public function __construct(Repository $repo, Uploader $uploader, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->uploader = $uploader;

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}


	/**
	 * @Route("/workout-feeds")
	 * @Method({"GET"})
	 */
	public function feedsAllAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');

		$data = $repo->findAll();

		return $this->render('feeds/all.html.twig', [
			'feeds' => $data
		]);
	}

	/**
	 * @Route("/workout-feeds/add")
	 * @Method({"GET"})
	 */
	public function feedsAddAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		return $this->render('feeds/add.html.twig');
	}

	/**
	 * @Route("/workout-feeds/add")
	 * @Method({"POST"})
	 */
	public function feedsPostAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$data = $request->request;

		$uploader = $path = $this->uploader->setFolder('workoutfeeds');

		if($data->has('id')) {
			$repo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');

			/** @var $feed WorkoutFeed */
			$feed = $repo->find($data->get('id'));

			if($request->files->get('image') !== null) {
				$path = $uploader->upload($request->files->get('image'));
				$feed->setFeaturedImage($path);
			}

			$feed->setTitle($data->get('title'));
			$feed->setContent($data->get('content'));
		} else {
			$path = $uploader->upload($request->files->get('image'));
			$feed = new WorkOutFeed($data->get('title'), $data->get('content'), $path);
		}

		$this->em->persist($feed);

		if($data->has('isSlider')) {
			$slider = $this->findSliderBelongTo($feed);

			if($slider === false) {
				$slider = new FeedSlider($feed);
				$this->em->persist($slider);
			}
		} else {
			$slider = $this->findSliderBelongTo($feed);

			if($slider !== false) {
				$feed->removeSlider();

				$this->em->remove($slider);
			}
		}

		$this->em->flush();

		if($data->has('id')) {
			$id = $data->get('id');

			return $this->redirectWithFlash("/admin/workout-feeds/edit/{$id}", $this->session, [
				'message' => [
					'code' => 200,
					'type' => 'update'
				]
			]);
		}

		return $this->redirectWithFlash($request->getRequestUri(), $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'insert'
			]
		]);
	}

	public function findSliderBelongTo(WorkOutFeed $feed) {
		$repo = $this->repo->getRepositoryOf('Slider');
		$sliders = $repo->findAll();

		$collection = new ArrayCollection($sliders);
		$slider = $collection->filter(function(Slider $slider) use($feed) {
			return ($slider instanceof FeedSlider)
			and ($slider->getReference()->getId() === $feed->getId());
		})->first();

		return $slider;
	}

	/**
	 * @Route("/workout-feeds/edit/{id}")
	 * @Method({"GET"})
	 */
	public function feedsEditAction(Request $request, $id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');

		$data = $repo->find($id);

		return $this->render('feeds/add.html.twig', [
			'feed' => $data
		]);
	}

	/**
	 * @Route("/workout-feeds/delete/{id}")
	 * @Method({"GET"})
	 */
	public function feedsDeleteAction(Request $request, $id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');

		/** @var $data WorkOutFeed */
		$data = $repo->find($id);

		if($data->getSlider() != null) {
			$this->em->remove($data->getSlider());
		}

		$this->em->remove($data);
		$this->em->flush();

		return $this->redirectWithFlash('/admin/workout-feeds', $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'delete'
			]
		]);
	}
}
