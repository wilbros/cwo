<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Step
 * @package Chef\DomainBundle\Entity\ChefRecipe
 * @ORM\Entity
 */
class Step extends AbstractEntity {

	/**
	 * @ORM\ManyToOne(targetEntity="Presentation", inversedBy="steps")
	 * @ORM\JoinColumn(name="presentation", referencedColumnName="id")
	 */
	private $presentation;

	/** @ORM\Column(type="string") */
	private $content;

	public function __construct(Presentation $presentation, $content) {
		parent::__construct();
		$this->presentation = $presentation;
		$this->content = $content;
	}

	/**
	 * @return mixed
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param mixed $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}
} 