<?php

namespace Chef\DomainBundle\Entity\Page;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Point extends AbstractEntity {

	/**
	 * @ORM\Column(type="string")
	 * @ORM\Id
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Page", inversedBy="points")
	 * @ORM\JoinColumn(name="Page", referencedColumnName="id")
	 */
	private $page;

	/** @ORM\Column(type="string", unique=true) */
	private $alias;

	/** @ORM\Column(type="string", length=5000) */
	private $content;

	public function __construct(Page $page, $alias, $content = '') {
		parent::__construct();

		$this->id = $alias;

		$this->page = $page;

		$this->alias = $alias;

		$this->content = $content;
	}

	/**
	 * @return mixed
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param mixed $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return mixed
	 */
	public function getAlias()
	{
		return $this->alias;
	}

	/**
	 * @param mixed $alias
	 */
	public function setAlias($alias)
	{
		$this->alias = $alias;
	}

	/**
	 * @return mixed
	 */
	public function getPage()
	{
		return $this->page;
	}

	/**
	 * @param mixed $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}
}