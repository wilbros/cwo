<?php

namespace Chef\AdminBundle\Service;

class Sluggable {
	static function slug($string) {
		$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
		return $slug."-".uniqid();
	}
}