<?php
namespace Chef\AdminBundle\Resources\extension;

use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Repository\Repository;
use Symfony\Component\DependencyInjection\Container;

class AppExtension extends \Twig_Extension {

	private $repo;

	public function __construct(Container $container) {
		if($container->isScopeActive('request')) {
			$this->repo = $container->get('repository');
		}
	}

	public function getGlobals() {
		/** @var $repo Repository */
		$repo = $this->repo->getRepositoryOf('Connect\Connect');

		/** @var $connect Connect */
		$connect = $repo->findOneBy([]);

		return array(
			'media' => [
				'twitter' => $connect->getTwitter(),
				'facebook' => $connect->getFacebook(),
				'instagram' => $connect->getInstagram(),
			]
		);
	}

	public function getName() {
		return 'app_extension';
	}
}