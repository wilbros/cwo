<?php

namespace Chef\DomainBundle\Entity\FoodGallery;

use Chef\AdminBundle\Service\Sluggable;
use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class FoodGallery
 * @package Chef\DomainBundle\Entity\FoodGallery
 * @ORM\Entity
 */
class FoodGallery extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $title;

	/** @ORM\Column(type="string") */
	private $slug;

	/**
	 * @ORM\ManyToOne(targetEntity="CategoryGallery")
	 * @ORM\JoinColumn(name="category", referencedColumnName="id")
	 */
	private $category;

	/** @ORM\Column(type="string") */
	private $featuredImage;

	/** @ORM\Column(type="string") */
	private $description;

	public function __construct($title, CategoryGallery $categoryGallery, $image) {
		parent::__construct();

		$this->setTitle($title);
		$this->slug = Sluggable::slug($title);
		$this->category = $categoryGallery;
		$this->featuredImage = $image;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;


	}

	public function getSlug() {
		return $this->slug;
	}

	public function getCategory() {
		return $this->category;
	}

	public function setCategory($category) {
		$this->category = $category;
	}

	public function getFeaturedImage() {
		return $this->featuredImage;
	}

	public function setFeaturedImage($featuredImage) {
		$this->featuredImage = $featuredImage;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

} 