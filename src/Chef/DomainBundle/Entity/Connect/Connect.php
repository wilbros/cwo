<?php

namespace Chef\DomainBundle\Entity\Connect;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Connect
 * @package Chef\DomainBundle\Entity\Connect
 * @ORM\Entity
 */
class Connect extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $phone;

	/** @ORM\Column(type="string") */
	private $email;

	/** @ORM\Column(type="text") */
	private $address;

	/** @ORM\Column(type="string") */
	private $content;

	/** @ORM\Column(type="string") */
	private $twitter;

	/** @ORM\Column(type="string") */
	private $facebook;

	/** @ORM\Column(type="string") */
	private $instagram;

	public function __construct() {
		parent::__construct();

		$this->phone = '';
		$this->email = '';
		$this->address = '';
		$this->content = '';
		$this->twitter = '';
		$this->facebook = '';
		$this->instagram = '';
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone($phone) {
		$this->phone = $phone;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function getTwitter() {
		return $this->twitter;
	}

	public function setTwitter($twitter) {
		$this->twitter = $twitter;
	}

	public function getFacebook() {
		return $this->facebook;
	}

	public function setFacebook($facebook) {
		$this->facebook = $facebook;
	}

	public function getInstagram() {
		return $this->instagram;
	}

	public function setInstagram($instagram) {
		$this->instagram = $instagram;
	}

} 