<?php

namespace Chef\FrontBundle\Controller;

use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\FoodGallery\CategoryGallery;
use Chef\DomainBundle\Entity\FoodGallery\FoodGallery;
use Chef\DomainBundle\Entity\Page\Page;
use Chef\DomainBundle\Entity\Travel\CategoryTravel;
use Chef\DomainBundle\Entity\Travel\Travel;
use Chef\DomainBundle\Repository\Repository;
use Chef\FrontBundle\Model\TravelModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Type;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends BaseController {
	private $repo, $em, $session, $mail_config;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "mail_username"   = @JMS\Inject("%mailer_user%"),
	 *      "mail_password"  = @JMS\Inject("%mailer_password%"),
	 *      "mail_to"       = @JMS\Inject("%mailer_to%"),
	 *      "mail_subject"       = @JMS\Inject("%mailer_subject%"),
	 * })
	 */
	public function __construct(Repository $repo, Session $session, $mail_to, $mail_subject) {
		$this->repo = $repo;
		$this->em = $repo->getManager();

		$this->mail_config = [
			'to' => $mail_to,
			'subject' => $mail_subject
		];

		$this->session = $session;
	}

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function indexAction() {
	    $journalRepo = $this->repo->getRepositoryOf('Journal\Journal');
	    $journals = $journalRepo->findBy([], ['createdOn' => 'Desc'], 3);

	    $sliderRepo = $this->repo->getRepositoryOf('Slider');
	    $sliders = $sliderRepo->findBy([], ['createdOn' => 'Desc']);

        /** @var $homePoints Page */
        $homePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'home'
        ]);

        $journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'journal'
        ]);

		return $this->render('index.html.twig', [
			'journals' => $journals,
			'sliders' => $sliders,
            'points' => [
                'home' => $homePage->getPoints(),
                'journal' => $journalPage->getPoints()
            ]
		]);
    }

	/**
	 * @Route("/chef")
	 * @Method({"GET"})
	 */
	public function chefAction() {
        /** @var $chefPage Page */
        $chefPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'chef'
        ]);

		return $this->render('chef.html.twig', [
            'points' => [
                'chef' => $chefPage->getPoints()
            ]
        ]);
	}

	/**
	 * @Route("/connect")
	 * @Method({"GET"})
	 */
	public function connectAction() {
		$connectRepo = $this->repo->getRepositoryOf('Connect\Connect');

		$data = $connectRepo->findOneBy([]);

		$connectPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'connect'
		]);

		return $this->render('connect.html.twig', [
			'connect' => $data,
			'points' => [
				'connect' => $connectPage->getPoints(),
			]
		]);
	}

	/**
	 * @Route("/connect")
	 * @Method({"POST"})
	 */
	public function cownnecdtPostAction(Request $request) {
		$mailer = $this->get('mailer');
		$message = $mailer->createMessage()
			->setSubject($this->mail_config['subject'])
			->setFrom($request->request->get('email'))
			->setTo($this->mail_config['to'])
			->setContentType('text/html')
			->setBody(
				$this->renderView('mail.html.twig', [
					'chef' => 'Nicola Russo',
					'name' => $request->request->get('name'),
					'email' => $request->request->get('email'),
					'from' => $request->request->get('from'),
					'message' => $request->request->get('message')
				]), 'text/html',  'utf-8'
			);

        $res = $mailer->send($message) ? 1 : 2;

        return new JsonResponse($res);
	}

	/**
	 * @Route("/cuisine")
	 * @Method({"GET"})
	 */
	public function cuisineAction() {
		$journalRepo = $this->repo->getRepositoryOf('Journal\Journal');
		$journals = $journalRepo->findBy([], ['createdOn' => 'Desc'], 3);

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

		$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');

		$categories = $categoryRepo->findAll();

		$list = [];
		/** @var $category CategoryGallery */
		foreach($categories as $category) {
			/** @var $gallery FoodGallery */
			$gallery = $galleryRepo->findOneBy(['category' => $category->getId()], ['createdOn' => 'Desc'], 1);

			$list[$gallery->getCategory()->getAlias()] = $gallery;
		}

		$recipesRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		$recipe = $recipesRepo->findOneBy([], ['createdOn' => 'Desc']);

		$healthyRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');
		$healthy = $healthyRepo->findOneBy([], ['createdOn' => 'Desc']);

		$journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'journal'
		]);

		$cuisinePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'cuisine'
		]);

		$galleryPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'gallery'
		]);

		$healthyPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'healthy'
		]);

		$recipePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'recipe'
		]);

		return $this->render('cuisine.html.twig', [
			'journals' => $journals,
			'latest' => $list,
			'recipe' => $recipe,
			'healthy' => $healthy,
			'points' => [
				'journal' => $journalPage->getPoints(),
				'cuisine' => $cuisinePage->getPoints(),
				'gallery' => $galleryPage->getPoints(),
				'healthy' => $healthyPage->getPoints(),
				'recipe' => $recipePage->getPoints(),
			]
		]);
	}

	/**
	 * @Route("/workout")
	 * @Method({"GET"})
	 */
	public function workoutAction() {
		$feedRepo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');
		$feeds = $feedRepo->findBy([], ['createdOn' => 'Desc'], 6);

		$journalRepo = $this->repo->getRepositoryOf('Journal\Journal');
		$journals = $journalRepo->findBy([], ['createdOn' => 'Desc'], 3);

        /** @var $introductionPage Page */
        $introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'wointroduction'
        ]);

        /** @var $wohi Page */
        $wohi = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'wohighintensity'
        ]);

        /** @var $wofd Page */
        $wofd = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'wofdfitness'
        ]);

        /** @var $feedPage Page */
        $feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'wofeed'
        ]);

		/** @var $globalWoPage Page */
		$globalWoPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'woglobal'
		]);

		$journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'journal'
		]);

		return $this->render('workout.html.twig', [
			'feeds' => $feeds,
			'journals' => $journals,
            'points' => [
                'introduction' => $introductionPage->getPoints(),
				'journal' => $journalPage->getPoints(),
                'feed' => $feedPage->getPoints(),
                'wohi' => $wohi->getPoints(),
                'wofd' => $wofd->getPoints(),
				'global' => $globalWoPage->getPoints()
            ]
		]);
	}

	/**
	 * @Route("/workout/feeds")
	 * @Method({"GET"})
	 */
	public function workoutFeedsAction(Request $request) {
		$r = $request->query;
		$page =  !$r->has('page') ? 1 : $r->get('page');

		$limit = 9;
		$offset = $page * $limit - $limit;

		$feedRepo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');
		$feeds = $feedRepo->findBy([], ['createdOn' => 'Desc'], $limit, $offset);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		$count = $feedRepo->createQueryBuilder('j')
			->select('COUNT(j.id)')
			->where('j.removedOn IS NULL')
			->getQuery()
			->getSingleScalarResult();

		return $this->render('workout-feeds.html.twig', [
			'feeds' => $feeds,
			'points' => [
				'feed' => $feedPage->getPoints()
			],
			'pagination' => [
				'url' => '/workout/feeds?page=',
				'page' => $page,
				'lastPage' => ceil($count / $limit)
			]
		]);
	}

	/**
	 * @Route("/gallery-oneplusone")
	 * @Method({"GET"})
	 */
	public function gallery112Action(Request $request) {
        $r = $request->query;

		$page =  !$r->has('page') ? 1 : $r->get('page');

		$limit = 5;
		$offset = $page * $limit - $limit;

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');
		$data = $galleryRepo->findBy([
			'category' => 2
		], ['createdOn' => 'desc'], $limit, $offset);

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

		$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');

		$categories = $categoryRepo->findAll();

		$count = $galleryRepo->createQueryBuilder('j')
			->select('COUNT(j.id)')
			->where('j.removedOn IS NULL AND j.category = 2')
			->getQuery()
			->getSingleScalarResult();

		$list = [];
		/** @var $category CategoryGallery */
		foreach($categories as $category) {
			/** @var $gallery FoodGallery */
			$gallery = $galleryRepo->findOneBy(['category' => $category->getId()], ['createdOn' => 'Desc'], 1);

			$list[$gallery->getCategory()->getAlias()] = $gallery;
		}

		$connectCompanyRepo = $this->repo->getRepositoryOf('Connect\ConnectCompany');

		return $this->render('gallery112.html.twig', [
			'images' => $data,
			'latest' => $list,
			'pagination' => [
				'url' => '/gallery-oneplusone?page=',
				'page' => $page,
				'lastPage' => ceil($count / $limit)
			],
			'company' => $connectCompanyRepo->findOneBy([])
		]);
	}

	/**
	 * @Route("/gallery-modern")
	 * @Method({"GET"})
	 */
	public function galleryModernAction(Request $request) {
        $r = $request->query;

		$page =  !$r->has('page') ? 1 : $r->get('page');

		$limit = 5;
		$offset = $page * $limit - $limit;

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');
		$data = $galleryRepo->findBy([
			'category' => 1
		], ['createdOn' => 'desc'], $limit, $offset);

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

		$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');

		$categories = $categoryRepo->findAll();

		$count = $galleryRepo->createQueryBuilder('j')
			->select('COUNT(j.id)')
			->where('j.removedOn IS NULL AND j.category = 1')
			->getQuery()
			->getSingleScalarResult();

		$list = [];
		/** @var $category CategoryGallery */
		foreach($categories as $category) {
			/** @var $gallery FoodGallery */
			$gallery = $galleryRepo->findOneBy(['category' => $category->getId()], ['createdOn' => 'Desc'], 1);

			$list[$gallery->getCategory()->getAlias()] = $gallery;
		}

		$connectCompanyRepo = $this->repo->getRepositoryOf('Connect\ConnectCompany');

		return $this->render('gallery-modern.html.twig', [
			'images' => $data,
			'latest' => $list,
			'pagination' => [
				'url' => '/gallery-modern?page=',
				'page' => $page,
				'lastPage' => ceil($count / $limit)
			],
			'company' => $connectCompanyRepo->findOneBy([])
		]);
	}

    /**
     * @Route("/gallery-mirror")
     * @Method({"GET"})
     */
    public function galleryMirrorAction(Request $request) {
        $r = $request->query;

        $page =  !$r->has('page') ? 1 : $r->get('page');

        $limit = 5;
        $offset = $page * $limit - $limit;

        $galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');
        $data = $galleryRepo->findBy([
            'category' => 4
        ], ['createdOn' => 'desc'], $limit, $offset);

        $galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

        $categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');

        $categories = $categoryRepo->findAll();

        $count = $galleryRepo->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->where('j.removedOn IS NULL AND j.category = 4')
            ->getQuery()
            ->getSingleScalarResult();

        $list = [];
        /** @var $category CategoryGallery */
        foreach($categories as $category) {
            /** @var $gallery FoodGallery */
            $gallery = $galleryRepo->findOneBy(['category' => $category->getId()], ['createdOn' => 'Desc'], 1);

            $list[$gallery->getCategory()->getAlias()] = $gallery;
        }

        $connectCompanyRepo = $this->repo->getRepositoryOf('Connect\ConnectCompany');

        return $this->render('gallery-mirror.html.twig', [
            'images' => $data,
            'latest' => $list,
            'pagination' => [
                'url' => '/gallery-mirror?page=',
                'page' => $page,
                'lastPage' => ceil($count / $limit)
            ],
            'company' => $connectCompanyRepo->findOneBy([])
        ]);
    }

	/**
	 * @Route("/gallery-deconstruction")
	 * @Method({"GET"})
	 */
	public function galleryDeconstructionAction(Request $request) {
		$r = $request->query;

		$page =  !$r->has('page') ? 1 : $r->get('page');

		$limit = 5;
		$offset = $page * $limit - $limit;

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');
		$data = $galleryRepo->findBy([
			'category' => 3
		], ['createdOn' => 'desc'], $limit, $offset);

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

		$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');

		$categories = $categoryRepo->findAll();

		$count = $galleryRepo->createQueryBuilder('j')
			->select('COUNT(j.id)')
			->where('j.removedOn IS NULL AND j.category = 3')
			->getQuery()
			->getSingleScalarResult();

		$list = [];
		/** @var $category CategoryGallery */
		foreach($categories as $category) {
			/** @var $gallery FoodGallery */
			$gallery = $galleryRepo->findOneBy(['category' => $category->getId()], ['createdOn' => 'Desc'], 1);

			$list[$gallery->getCategory()->getAlias()] = $gallery;
		}

		$connectCompanyRepo = $this->repo->getRepositoryOf('Connect\ConnectCompany');

		return $this->render('gallery-dec.html.twig', [
			'images' => $data,
			'latest' => $list,
			'pagination' => [
				'url' => '/gallery-deconstruction?page=',
				'page' => $page,
				'lastPage' => ceil($count / $limit)
			],
			'company' => $connectCompanyRepo->findOneBy([])
		]);
	}

	/**
	 * @Route("/recipes")
	 * @Method({"GET"})
	 */
	public function recipesAction(Request $request) {
        $r = $request->query;

        $repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
        $dinnerMenus = $repoDinner->findAll();

        $repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
        $recipeMenus =  $repoRecipe->findAll();

        $tagRepo = $this->repo->getRepositoryOf('ChefRecipe\Tag');
        $tags =  $tagRepo->findBy([], [], 10);

        $page =  !$r->has('page') ? 1 : $r->get('page');

        $limit = 10;
        $offset = $page * $limit - $limit;

        $recipesRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');

        $builder = $recipesRepo->createQueryBuilder('p');
        if($r->has('dmctg') and trim($r->get('dmctg')) != '') {
            $ctg = $repoDinner->findOneBy([
                'name' => $r->get('dmctg')
            ]);

            if($ctg != null) {
                $builder->join('p.dinnerMenu', 'd')->where(
                    $builder->expr()->eq('d.id', $ctg->getId())
                );
            }
        }

        if($r->has('rmctg') and trim($r->get('rmctg')) != '') {
            $ctg = $repoRecipe->findOneBy([
                'name' => $r->get('rmctg')
            ]);

            if($ctg != null) {
                $builder->join('p.recipeMenu', 'r')->where(
                    $builder->expr()->eq('r.id', $ctg->getId())
                );
            }
        }

        if($r->has('tag') and trim($r->get('tag')) != '') {
            $tag = $tagRepo->findOneBy([
                'name' => $r->get('tag')
            ]);

            if($tag != null) {
                $builder->join('p.tags', 't')->where(
                    $builder->expr()->eq('t.id', $tag->getId())
                );
            }
        }

        $recipes = $builder->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('p.createdOn', 'DESC')
            ->getQuery()
            ->execute();

        $recipePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'recipe'
        ]);

        $count = $recipesRepo->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->where('j.removedOn IS NULL')
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('recipes.html.twig', [
            'recipes' => $recipes,
            'dinnerMenus' => $dinnerMenus,
            'recipeMenus' => $recipeMenus,
            'tags' => $tags,
            'points' => [
                'recipe' => $recipePage->getPoints(),
            ],
            'pagination' => [
                'url' => '/recipes?page=',
                'page' => $page,
                'lastPage' => ceil($count / $limit)
            ]
        ]);
	}

	/**
	 * @Route("/recipes/{slug}")
	 * @Method({"GET"})
	 */
	public function recipeDetailAction($slug) {
		$recipesRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		$recipe = $recipesRepo->findOneBy([
			'slug' => $slug
		]);

		$recipes = $recipesRepo->createQueryBuilder('p')
			->where('p.id <> :id')
			->setParameter('id', $recipe->getId(), Type::INTEGER)
			->setMaxResults(3)
			->getQuery()->execute();

		$repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
		$dinnerMenus = $repoDinner->findAll();

		$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
		$recipeMenus =  $repoRecipe->findAll();

		$tagRepo = $this->repo->getRepositoryOf('ChefRecipe\Tag');
		$tags =  $tagRepo->findBy([], [], 10);

		return $this->render('recipe-page.html.twig', [
			'recipe' => $recipe,
			'related' => $recipes,
			'dinnerMenus' => $dinnerMenus,
			'recipeMenus' => $recipeMenus,
			'tags' => $tags
		]);
	}

	/**
	 * @Route("/healthy")
	 * @Method({"GET"})
	 */
	public function healthyAction(Request $request) {

        $r = $request->query;

		$page =  !$r->has('page') ? 1 : $r->get('page');

		$limit = 10;
		$offset = $page * $limit - $limit;

		$healthRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');


        $repoDinner = $this->repo->getRepositoryOf('HealthyRecipe\DinnerMenuHealthy');
        $repoRecipe = $this->repo->getRepositoryOf('HealthyRecipe\RecipeMenuHealthy');
        $tagRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyTag');

		$healthyPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'healthy'
		]);

        $builder = $healthRepo->createQueryBuilder('p');
        if($r->has('dmctg') and trim($r->get('dmctg')) != '') {
            $ctg = $repoDinner->findOneBy([
                'name' => $r->get('dmctg')
            ]);

            if($ctg != null) {
                $builder->join('p.dinnerMenu', 'd')->where(
                    $builder->expr()->eq('d.id', $ctg->getId())
                );
            }
        }

        if($r->has('rmctg') and trim($r->get('rmctg')) != '') {
            $ctg = $repoRecipe->findOneBy([
                'name' => $r->get('rmctg')
            ]);

            if($ctg != null) {
                $builder->join('p.recipeMenu', 'r')->where(
                    $builder->expr()->eq('r.id', $ctg->getId())
                );
            }
        }

        if($r->has('tag') and trim($r->get('tag')) != '') {
            $tag = $tagRepo->findOneBy([
                'name' => $r->get('tag')
            ]);

            if($tag != null) {
                $builder->join('p.tags', 't')->where(
                    $builder->expr()->eq('t.id', $tag->getId())
                );
            }
        }

        $recipes = $builder->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('p.createdOn', 'DESC')
            ->getQuery()
            ->execute();

		$count = $healthRepo->createQueryBuilder('j')
			->select('COUNT(j.id)')
			->where('j.removedOn IS NULL')
			->getQuery()
			->getSingleScalarResult();

		return $this->render('healthy.html.twig', [
			'recipes' => $recipes,
			'points' => [
				'healthy' => $healthyPage->getPoints(),
			],
			'pagination' => [
				'url' => '/healthy?page=',
				'page' => $page,
				'lastPage' => ceil($count / $limit)
			]
		]);
	}

	/**
	 * @Route("/healthy/{slug}")
	 * @Method({"GET"})
	 */
	public function healthyDetailAction($slug) {
		$recipesRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');
		$recipe = $recipesRepo->findOneBy([
			'slug' => $slug
		]);

		$recipes = $recipesRepo->createQueryBuilder('p')
			->where('p.id <> :id')
			->setParameter('id', $recipe->getId(), Type::INTEGER)
			->setMaxResults(3)
			->getQuery()->execute();

        $repoDinner = $this->repo->getRepositoryOf('HealthyRecipe\DinnerMenuHealthy');
        $dinnerMenus = $repoDinner->findAll();

        $repoRecipe = $this->repo->getRepositoryOf('HealthyRecipe\RecipeMenuHealthy');
        $recipeMenus =  $repoRecipe->findAll();

		return $this->render('healthy-page.html.twig', [
			'recipe' => $recipe,
			'related' => $recipes,
            'dinnerMenus' => $dinnerMenus,
            'recipeMenus' => $recipeMenus
		]);
	}

	/**
	 * @Route("/journals")
	 * @Method({"GET"})
	 */
	public function journalsAction(Request $request) {
        $r = $request->query;

        $page =  !$r->has('page') ? 1 : $r->get('page');

        $limit = 10;
        $offset = $page * $limit - $limit;

        $journalRepo = $this->repo->getRepositoryOf('Journal\Journal');

        $tagRepo = $this->repo->getRepositoryOf('Journal\JournalTag');

        $journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'journal'
        ]);

        $builder = $journalRepo->createQueryBuilder('p');
        if($r->has('tag') and trim($r->get('tag')) != '') {
            $tag = $tagRepo->findOneBy([
                'name' => $r->get('tag')
            ]);

            if($tag != null) {
                $builder->join('p.tags', 't')->where(
                    $builder->expr()->eq('t.id', $tag->getId())
                );
            }
        }

        $journals = $builder->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('p.createdOn', 'DESC')
            ->getQuery()
            ->execute();

        $count = $journalRepo->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->where('j.removedOn IS NULL')
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('journals.html.twig', [
            'journals' => $journals,
            'points' => [
                'journal' => $journalPage->getPoints(),
            ],
            'pagination' => [
                'url' => '/journals?page=',
                'page' => $page,
                'lastPage' => ceil($count / $limit)
            ]
        ]);
	}

	/**
	 * @Route("/journals/{slug}")
	 * @Method({"GET"})
	 */
	public function journalDetailAction($slug) {
		$repo = $this->repo->getRepositoryOf('Journal\Journal');
		$journal = $repo->findOneBy([
			'slug' => $slug
		]);

        $builder = $repo->createQueryBuilder('p');
		$journals = $builder
			->where('p.id <> :id')
			->setParameter('id', $journal->getId(), Type::INTEGER)
			->setMaxResults(3)
			->getQuery()->execute();

        $latestJournals = $builder
            ->where('p.id <> :id')
            ->setParameter('id', $journal->getId(), Type::INTEGER)
            ->setMaxResults(5)
            ->orderBy('p.createdOn', 'Desc')
            ->getQuery()
            ->execute();

		return $this->render('journal-page.html.twig', [
			'journal' => $journal,
			'related' => $journals,
            'latest' => $latestJournals
		]);
	}

	/**
	 * @Route("/travel")
	 * @Method({"GET"})
	 */
	public function travelAction(Request $request) {
		return $this->render('travel.html.twig');
	}

	/**
	 * @Route("/gdata/markers")
	 * @Method({"GET"})
	 */
	public function travelGdataMarkersAction(Request $request) {
		$repo = $this->repo->getRepositoryOf('Travel\CategoryTravel');

		$markers = (new ArrayCollection($repo->findAll()))->map(function(CategoryTravel $ctg) {
			return $ctg->getName();
		});

		return new JsonResponse([
			'markers' => $markers->toArray()
		]);
	}

    /**
     * @Route("/gdata")
     * @Method({"GET"})
     */
    public function travelGdataAction(Request $request) {
		$useragent = $request->headers->get('user-agent');

		if($request->isXmlHttpRequest()) {
			if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
				$repo = $this->repo->getRepositoryOf('Travel\Travel');
				$data = (new ArrayCollection($repo->findAll()))->map(function(Travel $travel) {
					$marker = "/templates/fleava_front/img/marker-".$travel->getCategory()->getAlias().".png";
					return [
						'address' => $travel->getGmapKeyword(),
						'data' => $travel->getWebsite(),
						'tag' => $travel->getCategory()->getAlias(),
						'options' => [
							'icon' => $marker
						]
					];
				});
				return new JsonResponse([
					'maps' => $data->toArray(),
					'type' => 'mobile'
				]);
			} else {
				$repo = $this->repo->getRepositoryOf('Travel\Travel');
				$data = (new ArrayCollection($repo->findAll()))->map(function(Travel $travel) {
					$marker = "/templates/fleava_front/img/marker-".$travel->getCategory()->getAlias().".png";
					return [
						'address' => $travel->getGmapKeyword(),
						'data' => $this->renderView('popup.html.twig', [
							'travel' => $travel
						]),
						'tag' => $travel->getCategory()->getAlias(),
						'options' => [
							'icon' => $marker
						]
					];
				});

				return new JsonResponse([
					'maps' => $data->toArray(),
					'type' => 'browser'
				]);
			}
		}


    }
}
