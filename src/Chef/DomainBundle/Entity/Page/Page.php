<?php

namespace Chef\DomainBundle\Entity\Page;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Page extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $route;

	/**
	 * @ORM\OneToMany(targetEntity="Point", mappedBy="page", indexBy="id", cascade={"all"})
	 */
	private $points;

	public function __construct($route) {
		parent::__construct();

		$this->setRoute($route);

		$this->points = new ArrayCollection();
	}

	public function getRoute() {
		return $this->route;
	}

	public function setRoute($route) {
		$this->route = $route;
	}

	public function getPoints() {
		return $this->points;
	}

	public function getPoint($alias) {
		return $this->points->get($alias);
	}

	public function addPoint(Point $point) {
		$this->points->set($point->getAlias(), $point);

		return $this;
	}
}