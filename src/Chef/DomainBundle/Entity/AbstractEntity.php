<?php

namespace Chef\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractEntity
 * @package Compnet\DomainBundle\Domain
 *
 * @ORM\MappedSuperclass;
 */
abstract class AbstractEntity {

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $createdOn;

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $updatedOn;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $removedOn = null;

	public function __construct() {
		$this->createdOn = new \DateTime('NOW');
		$this->updatedOn = new \DateTime('NOW');
	}

	public function getId() {
		return $this->id;
	}

	public function getCreatedOn() {
		return $this->createdOn->format('D, d M Y');
	}

	public function getUpdatedOn() {
		return $this->updatedOn;
	}

	public function setUpdatedOn($updatedOn) {
		$this->updatedOn = $updatedOn;
	}

    public function getRemovedOn() {
        return $this->removedOn;
    }

    public function remove(\DateTime $dateTime = null) {
        $this->removedOn = $dateTime ?: new \DateTime('NOW');

        return $this;
    }

    public function isRemoved() {
        return $this->removedOn !== null;
    }
}