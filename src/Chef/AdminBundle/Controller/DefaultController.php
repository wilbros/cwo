<?php

namespace Chef\AdminBundle\Controller;

use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\Connect\ConnectCompany;
use Chef\DomainBundle\Repository\Repository;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class DefaultController extends BaseController {
	private $repo, $em, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository")
	 * })
	 */
	public function __construct(Repository $repo, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function indexAction() {
        if(!$this->session->has(SESSION_KEY)) {
            return new RedirectResponse('/admin/login');
        }

        $connectRepo = $this->repo->getRepositoryOf('Connect\Connect');

        if(!$connect = $connectRepo->findOneBy([])) {
            $connect = new Connect();
        }

        return $this->render('connect/add.html.twig', [
            'connect' => $connect
        ]);
    }

	/**
	 * @Route("/connect")
	 * @Method({"GET"})
	 */
	public function connectAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$connectRepo = $this->repo->getRepositoryOf('Connect\Connect');

		if(!$connect = $connectRepo->findOneBy([])) {
			$connect = new Connect();
		}

		return $this->render('connect/add.html.twig', [
			'connect' => $connect
		]);
	}

	/**
	 * @Route("/connect")
	 * @Method({"POST"})
	 */
	public function connectPostAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$connectRepo = $this->repo->getRepositoryOf('Connect\Connect');

		$form = $request->request;

		if(!$connect = $connectRepo->findOneBy([])) {
			$connect = new Connect();
		}

		$connect->setPhone($form->get('phone'));
		$connect->setEmail($form->get('email'));
		$connect->setAddress($form->get('address'));
		$connect->setContent($form->get('content'));

		$connect->setTwitter($form->get('twitter'));
		$connect->setFacebook($form->get('facebook'));
		$connect->setInstagram($form->get('instagram'));

		$this->em->persist($connect);
		$this->em->flush();

		return $this->redirectWithFlash("/admin/connect", $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'update'
			]
		]);
	}

	/**
	 * @Route("/connect-company")
	 * @Method({"GET"})
	 */
	public function connectCompanyAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$connectRepo = $this->repo->getRepositoryOf('Connect\ConnectCompany');

		if(!$connect = $connectRepo->findOneBy([])) {
			$connect = new ConnectCompany();
		}

		return $this->render('connect/add_company.html.twig', [
			'connect' => $connect
		]);
	}

	/**
	 * @Route("/connect-company")
	 * @Method({"POST"})
	 */
	public function connectCompanyPostAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$connectRepo = $this->repo->getRepositoryOf('Connect\ConnectCompany');

		$form = $request->request;

		if(!$connect = $connectRepo->findOneBy([])) {
			$connect = new ConnectCompany();
		}

		$connect->setTitle($form->get('title'));
		$connect->setCompany($form->get('company'));
		$connect->setPhone($form->get('phone'));
		$connect->setFax($form->get('fax'));

		$connect->clearEmail();
		foreach($form->get('email') as $email) {
			if(strlen($email) > 1) {
				$connect->addEmail($email);
			}
		}

		$connect->setAddress($form->get('address'));

		$this->em->persist($connect);
		$this->em->flush();

		return $this->redirectWithFlash("/admin/connect-company", $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'update'
			]
		]);
	}
}
