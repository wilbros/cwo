<?php

namespace Chef\AdminBundle\Controller;

use Chef\AdminBundle\Service\Uploader\Uploader;
use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\FoodGallery\CategoryGallery;
use Chef\DomainBundle\Entity\FoodGallery\FoodGallery;
use Chef\DomainBundle\Entity\Page\Page;
use Chef\DomainBundle\Entity\Page\Point;
use Chef\DomainBundle\Repository\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class PagesController extends BaseController {
	private $repo, $em, $uploader, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "uploader" = @JMS\Inject("uploader.local")
	 * })
	 */
	public function __construct(Repository $repo, Uploader $uploader, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->uploader = $uploader;

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

	/**
	 * @Route("/pages")
	 * @Method({"GET"})
	 */
	public function pagesAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		return $this->render('pages/all.html.twig');
	}

    /**
     * @Route("/pages/home")
     * @Method({"GET"})
     */
    public function pageHomeAction() {
	    if(!$this->session->has(SESSION_KEY)) {
		    return new RedirectResponse('/admin/login');
	    }

		$journalRepo = $this->repo->getRepositoryOf('Journal\Journal');
	    $journals = $journalRepo->findBy([], ['createdOn' => 'Desc'], 3);

	    $sliderRepo = $this->repo->getRepositoryOf('Slider');
	    $sliders = $sliderRepo->findAll();

	    /** @var $homePoints Page */
	    $homePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
		    'route' => 'home'
	    ]);

	    $journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
		    'route' => 'journal'
	    ]);

		return $this->render('Pages/home.html.twig', [
			'journals' => $journals,
			'sliders' => $sliders,
			'points' => [
				'home' => $homePage->getPoints(),
				'journal' => $journalPage->getPoints()
			]
		]);
    }

	/**
	 * @Route("/pages/home")
	 * @Method({"POST"})
	 */
	public function pageHomePostAction(Request $request) {
		/** @var $homePoints Page */
		$homePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'home'
		]);

		/** @var $journalPage Page */
		$journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'journal'
		]);

		if($request->files->has('home_bg')) {
			$background = $request->files->get('home_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('homepage');
				$bg = $path->upload($background);

				$request->request->set('home_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $homePage->getPoint($k);

			if(!$point) {
				$point = $journalPage->getPoint($k);

				if(!$point) continue;
			}

			$point->setContent($i);
		}

		$this->em->persist($homePage);
		$this->em->persist($journalPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/home');
	}

	protected function updateGlobalWorkout(Request $request) {
		/** @var $globalWoPage Page */
		$globalWoPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'woglobal'
		]);

		$tab1Alias = 'wo_tab_1';
		if($request->request->has($tab1Alias)) {
			/** @var  $p Point */
			$p = $globalWoPage->getPoint($tab1Alias);
			$p->setContent($request->request->get($tab1Alias));
		}

		$tab2Alias = 'wo_tab_2';
		if($request->request->has($tab2Alias)) {
			/** @var  $p Point */
			$p = $globalWoPage->getPoint($tab2Alias);
			$p->setContent($request->request->get($tab2Alias));
		}

		$tab3Alias = 'wo_tab_3';
		if($request->request->has($tab3Alias)) {
			/** @var  $p Point */
			$p = $globalWoPage->getPoint($tab3Alias);
			$p->setContent($request->request->get($tab3Alias));
		}

		return $globalWoPage;
	}

	/**
	 * @Route("/pages/workout-introduction")
	 * @Method({"POST"})
	 */
	public function wtroPostAction(Request $request) {
		/** @var $introductionPage Page */
		$introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wointroduction'
		]);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		$globalWoPage = $this->updateGlobalWorkout($request);

		if($request->files->has('woin_bg')) {
			$background = $request->files->get('woin_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$request->request->set('woin_bg', $bg);
			}
		}

		if($request->files->has('woin_mid_bg2')) {
			$background = $request->files->get('woin_mid_bg2');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$request->request->set('woin_mid_bg2', $bg);
			}
		}

		if($request->files->has('woin_ns_bg1')) {
			$background = $request->files->get('woin_ns_bg1');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$request->request->set('woin_ns_bg1', $bg);
			}
		}

		if($request->files->has('woin_ns_bg2')) {
			$background = $request->files->get('woin_ns_bg2');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$request->request->set('woin_ns_bg2', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $introductionPage->getPoint($k);

			if(!$point) {
				$point = $feedPage->getPoint($k);

				if(!$point) continue;
			}

			$point->setContent($i);
		}

		$this->em->persist($introductionPage);
		$this->em->persist($feedPage);
		$this->em->persist($globalWoPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/workout-introduction');
	}

	/**
	 * @Route("/pages/workout-introduction")
	 * @Method({"GET"})
	 */
	public function woinAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		/** @var $globalWoPage Page */
		$globalWoPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'woglobal'
		]);

		/** @var $introductionPage Page */
		$introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wointroduction'
		]);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		$feedRepo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');
		$feeds = $feedRepo->findBy([], ['createdOn' => 'Desc'], 6);

		return $this->render('Pages/workout-in.html.twig', [
			'feeds' => $feeds,
			'points' => [
				'introduction' => $introductionPage->getPoints(),
				'feed' => $feedPage->getPoints(),
				'global' => $globalWoPage->getPoints()
			]
		]);
	}

	/**
	 * @Route("/pages/workout-high-intensity-training")
	 * @Method({"GET"})
	 */
	public function wohiAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$feedRepo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');
		$feeds = $feedRepo->findBy([], ['createdOn' => 'Desc'], 6);

		/** @var $globalWoPage Page */
		$globalWoPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'woglobal'
		]);

		/** @var $introductionPage Page */
		$introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wointroduction'
		]);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		/** @var $wohi Page */
		$wohi = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wohighintensity'
		]);

		return $this->render('Pages/workout-hi.html.twig', [
			'feeds' => $feeds,
			'points' => [
				'introduction' => $introductionPage->getPoints(),
				'feed' => $feedPage->getPoints(),
				'wohi' => $wohi->getPoints(),
				'global' => $globalWoPage->getPoints()
			]
		]);
	}

	/**
	 * @Route("/pages/workout-high-intensity-training")
	 * @Method({"POST"})
	 */
	public function wo2xPostAction(Request $request) {
		/** @var $introductionPage Page */
		$introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wointroduction'
		]);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		/** @var $wohi Page */
		$wohi = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wohighintensity'
		]);

		$globalWoPage = $this->updateGlobalWorkout($request);

		if($request->files->has('woin_bg')) {
			$background = $request->files->get('woin_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$point = $introductionPage->getPoint('woin_bg');

				$point->setContent($bg);
			}
		}

		if($request->files->has('wohi_bg1')) {
			$background = $request->files->get('wohi_bg1');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-intensity');
				$bg = $path->upload($background);

				$request->request->set('wohi_bg1', $bg);
			}
		}

		if($request->files->has('wohi_low_bg')) {
			$background = $request->files->get('wohi_low_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-intensity');
				$bg = $path->upload($background);

				$request->request->set('wohi_low_bg', $bg);
			}
		}

		if($request->files->has('wohi_bg_foot1')) {
			$background = $request->files->get('wohi_bg_foot1');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-intensity');
				$bg = $path->upload($background);

				$request->request->set('wohi_bg_foot1', $bg);
			}
		}

		if($request->files->has('wohi_bg_foot2')) {
			$background = $request->files->get('wohi_bg_foot2');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-intensity');
				$bg = $path->upload($background);

				$request->request->set('wohi_bg_foot2', $bg);
			}
		}

		if($request->files->has('wohi_poster_bg')) {
			$background = $request->files->get('wohi_poster_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-intensity');
				$bg = $path->upload($background);

				$request->request->set('wohi_poster_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $wohi->getPoint($k);

			if(!$point) {
				$point = $feedPage->getPoint($k);

				if(!$point) continue;
			}

			$point->setContent($i);
		}

		$this->em->persist($introductionPage);
		$this->em->persist($wohi);
		$this->em->persist($feedPage);
		$this->em->persist($globalWoPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/workout-high-intensity-training');
	}

	/**
	 * @Route("/pages/workout-introduction")
	 * @Method({"POST"})
	 */
	public function wtsubPostAction(Request $request) {
		/** @var $introductionPage Page */
		$introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wointroduction'
		]);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		/** @var $globalWoPage Page */
		$globalWoPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'woglobal'
		]);

		$tab1Alias = 'wo_tab_1';
		if($request->request->has($tab1Alias)) {
			/** @var  $p Point */
			$p = $globalWoPage->getPoint($tab1Alias);
			$p->setContent($request->request->get($tab1Alias));
		}

		$tab2Alias = 'wo_tab_2';
		if($request->request->has($tab2Alias)) {
			/** @var  $p Point */
			$p = $globalWoPage->getPoint($tab2Alias);
			$p->setContent($request->request->get($tab2Alias));
		}

		$tab3Alias = 'wo_tab_3';
		if($request->request->has($tab3Alias)) {
			/** @var  $p Point */
			$p = $globalWoPage->getPoint($tab3Alias);
			$p->setContent($request->request->get($tab3Alias));
		}

		if($request->files->has('woin_bg')) {
			$background = $request->files->get('woin_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$request->request->set('woin_bg', $bg);
			}
		}

		if($request->files->has('woin_ns_bg2')) {
			$background = $request->files->get('woin_ns_bg2');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$request->request->set('woin_ns_bg2', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $introductionPage->getPoint($k);

			if(!$point) {
				$point = $feedPage->getPoint($k);

				if(!$point) continue;
			}

			$point->setContent($i);
		}

		$this->em->persist($introductionPage);
		$this->em->persist($feedPage);
		$this->em->persist($globalWoPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/workout-introduction');
	}

	/**
	 * @Route("/pages/workout-fitness-and-diet")
	 * @Method({"GET"})
	 */
	public function wofdAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$feedRepo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');
		$feeds = $feedRepo->findBy([], ['createdOn' => 'Desc'], 6);

		/** @var $globalWoPage Page */
		$globalWoPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'woglobal'
		]);

		/** @var $introductionPage Page */
		$introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wointroduction'
		]);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		/** @var $wofd Page */
		$wofd = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofdfitness'
		]);

		return $this->render('Pages/workout-fd.html.twig', [
			'feeds' => $feeds,
			'points' => [
				'introduction' => $introductionPage->getPoints(),
				'feed' => $feedPage->getPoints(),
				'wofd' => $wofd->getPoints(),
				'global' => $globalWoPage->getPoints()
			]
		]);
	}

	/**
	 * @Route("/pages/workout-fitness-and-diet")
	 * @Method({"POST"})
	 */
	public function wofd2CAction(Request $request) {
		/** @var $introductionPage Page */
		$introductionPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wointroduction'
		]);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		/** @var $wofd Page */
		$wofd = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofdfitness'
		]);

		$globalWoPage = $this->updateGlobalWorkout($request);

		if($request->files->has('woin_bg')) {
			$background = $request->files->get('woin_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-introduction');
				$bg = $path->upload($background);

				$point = $introductionPage->getPoint('woin_bg');

				$point->setContent($bg);
			}
		}

		if($request->files->has('wofd_rcp_bg1')) {
			$background = $request->files->get('wofd_rcp_bg1');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-fitness');
				$bg = $path->upload($background);

				$request->request->set('wofd_rcp_bg1', $bg);
			}
		}

		if($request->files->has('wofd_rcp_bg2')) {
			$background = $request->files->get('wofd_rcp_bg2');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-fitness');
				$bg = $path->upload($background);

				$request->request->set('wofd_rcp_bg2', $bg);
			}
		}

		if($request->files->has('wofd_rcp_bg3')) {
			$background = $request->files->get('wofd_rcp_bg3');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-fitness');
				$bg = $path->upload($background);

				$request->request->set('wofd_rcp_bg3', $bg);
			}
		}

		if($request->files->has('wofd_low_bg')) {
			$background = $request->files->get('wofd_low_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-fitness');
				$bg = $path->upload($background);

				$request->request->set('wofd_low_bg', $bg);
			}
		}

		if($request->files->has('wofd_lgm_bg')) {
			$background = $request->files->get('wofd_lgm_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('workout-fitness');
				$bg = $path->upload($background);

				$request->request->set('wofd_lgm_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $wofd->getPoint($k);

			if(!$point) {
				$point = $feedPage->getPoint($k);

				if(!$point) continue;
			}

			$point->setContent($i);
		}

		$this->em->persist($introductionPage);
		$this->em->persist($wofd);
		$this->em->persist($feedPage);
		$this->em->persist($globalWoPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/workout-fitness-and-diet');
	}

	/**
	 * @Route("/pages/chef")
	 * @Method({"GET"})
	 */
	public function chefAsAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

        /** @var $chefPage Page */
        $chefPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'chef'
        ]);

		return $this->render('Pages/chef.html.twig', [
            'points' => [
                'chefpage' => $chefPage->getPoints()
            ]
        ]);
	}

    /**
     * @Route("/pages/chef")
     * @Method({"POST"})
     */
    public function che12PostAsAction(Request $request) {
        /** @var $chefPage Page */
        $chefPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
            'route' => 'chef'
        ]);

        if($request->files->has('chef_bg')) {
            $background = $request->files->get('chef_bg');
            if($background != null) {
                $path = $this->uploader->setFolder('chefbg');
                $bg = $path->upload($background);

                $request->request->set('chef_bg', $bg);
            }
        }

        if($request->files->has('chef_mdl_bg')) {
            $background = $request->files->get('chef_mdl_bg');
            if($background != null) {
                $path = $this->uploader->setFolder('chefbg');
                $bg = $path->upload($background);

                $request->request->set('chef_mdl_bg', $bg);
            }
        }

        foreach($request->request->all() as $k => $i) {
            /** @var $point Point */
            $point = $chefPage->getPoint($k);

            $point->setContent($i);
        }

        $this->em->persist($chefPage);
        $this->em->flush();

        return new RedirectResponse('/admin/pages/chef');
    }

	/**
	 * @Route("/pages/cuisine")
	 * @Method({"GET"})
	 */
	public function cuisine21Action() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$journalRepo = $this->repo->getRepositoryOf('Journal\Journal');
		$journals = $journalRepo->findBy([], ['createdOn' => 'Desc'], 3);

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

		$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');

		$categories = $categoryRepo->findAll();

		$list = [];
		/** @var $category CategoryGallery */
		foreach($categories as $category) {
			/** @var $gallery FoodGallery */
			$gallery = $galleryRepo->findOneBy(['category' => $category->getId()], ['createdOn' => 'Desc'], 1);

			$list[$gallery->getCategory()->getAlias()] = $gallery;
		}

		$recipesRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');
		$recipe = $recipesRepo->findOneBy([], ['createdOn' => 'Desc']);

		$healthyRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');
		$healthy = $healthyRepo->findOneBy([], ['createdOn' => 'Desc']);

		$journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'journal'
		]);

		$cuisinePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'cuisine'
		]);

		$galleryPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'gallery'
		]);

		return $this->render('Pages/cuisine-adm.html.twig', [
			'journals' => $journals,
			'latest' => $list,
			'recipe' => $recipe,
			'healthy' => $healthy,
			'points' => [
				'journal' => $journalPage->getPoints(),
				'cuisine' => $cuisinePage->getPoints(),
				'gallery' => $galleryPage->getPoints()
			]
		]);
	}

	/**
	 * @Route("/pages/cuisine")
	 * @Method({"POST"})
	 */
	public function cuisin21eAsAction(Request $request) {
		$journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'journal'
		]);

		$cuisinePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'cuisine'
		]);

		$galleryPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'gallery'
		]);

		if($request->files->has('cuisine_bg')) {
			$background = $request->files->get('cuisine_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('cuisine_main_bg');
				$bg = $path->upload($background);

				$request->request->set('cuisine_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $cuisinePage->getPoint($k);

			if(!$point) {
				$point = $galleryPage->getPoint($k);
			}

			if(!$point) {
				$point = $journalPage->getPoint($k);
			}

			$point->setContent($i);
		}

		$this->em->persist($cuisinePage);
		$this->em->persist($galleryPage);
		$this->em->persist($journalPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/cuisine');
	}

	/**
	 * @Route("/pages/connect")
	 * @Method({"GET"})
	 */
	public function connectPageAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$connectRepo = $this->repo->getRepositoryOf('Connect\Connect');

		$data = $connectRepo->findOneBy([]);

		$connectPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'connect'
		]);

		return $this->render('Pages/connect.html.twig', [
			'connect' => $data,
			'points' => [
				'connect' => $connectPage->getPoints(),
			]
		]);
	}

	/**
	 * @Route("/pages/connect")
	 * @Method({"POST"})
	 */
	public function con2nectAction(Request $request) {
		$connectPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'connect'
		]);

		if($request->files->has('connect_bg')) {
			$background = $request->files->get('connect_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('chefbg');
				$bg = $path->upload($background);

				$request->request->set('connect_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $connectPage->getPoint($k);

			if(!$point) continue;

			$point->setContent($i);
		}

		$this->em->persist($connectPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/connect');
	}

	/**
	 * @Route("/pages/journal")
	 * @Method({"GET"})
	 */
	public function journalPageAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$r = $request->query;

		$limit = 10;
		$offset = !$r->has('page') ? 0 : $r->get('page') * $limit - $limit;

		$journalRepo = $this->repo->getRepositoryOf('Journal\Journal');
		$journals = $journalRepo->findBy([], ['createdOn' => 'Desc'], $limit, $offset);

		$journalPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'journal'
		]);

		return $this->render('Pages/journals.html.twig', [
			'journals' => $journals,
			'points' => [
				'journal' => $journalPage->getPoints(),
			]
		]);
	}

	/**
	 * @Route("/pages/journal")
	 * @Method({"POST"})
	 */
	public function journ2alAction(Request $request) {
		$connectPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'journal'
		]);

		if($request->files->has('journal_bg')) {
			$background = $request->files->get('journal_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('journal_main_bg');
				$bg = $path->upload($background);

				$request->request->set('journal_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $connectPage->getPoint($k);

			if(!$point) continue;

			$point->setContent($i);
		}

		$this->em->persist($connectPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/journal');
	}

	/**
	 * @Route("/pages/chef-recipe")
	 * @Method({"GET"})
	 */
	public function chefRecipePageAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$r = $request->query;

		$limit = 10;
		$offset = !$r->has('page') ? 0 : $r->get('page') * $limit - $limit;

		$recipesRepo = $this->repo->getRepositoryOf('ChefRecipe\ChefRecipe');

		$conditions = [];

		if($r->has('dmctg')) {
			$conditions['dinnerMenu'] = $r->get('dmctg');
		}

		if($r->has('rmctg')) {
			$conditions['recipeMenu'] = $r->get('rmctg');
		}

		if($r->has('tag')) {
			$tag = $r->get('tag');
		}

		$recipes = $recipesRepo->findBy($conditions, ['createdOn' => 'Desc'], $limit, $offset);

		$repoDinner = $this->repo->getRepositoryOf('ChefRecipe\DinnerMenu');
		$dinnerMenus = $repoDinner->findAll();

		$repoRecipe = $this->repo->getRepositoryOf('ChefRecipe\RecipeMenu');
		$recipeMenus =  $repoRecipe->findAll();

		$tagRepo = $this->repo->getRepositoryOf('ChefRecipe\Tag');
		$tags =  $tagRepo->findBy([], [], 10);

		$recipePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'recipe'
		]);

		return $this->render('Pages/recipes.html.twig', [
			'recipes' => $recipes,
			'dinnerMenus' => $dinnerMenus,
			'recipeMenus' => $recipeMenus,
			'tags' => $tags,
			'points' => [
				'recipe' => $recipePage->getPoints(),
			]
		]);
	}

	/**
	 * @Route("/pages/chef-recipe")
	 * @Method({"POST"})
	 */
	public function recipe2alAction(Request $request) {
		$recipePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'recipe'
		]);

		if($request->files->has('recipe_bg')) {
			$background = $request->files->get('recipe_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('recipe_main_bg');
				$bg = $path->upload($background);

				$request->request->set('recipe_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $recipePage->getPoint($k);

			if(!$point) continue;

			$point->setContent($i);
		}

		$this->em->persist($recipePage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/chef-recipe');
	}

	/**
	 * @Route("/pages/healthy")
	 * @Method({"GET"})
	 */
	public function healthyPageAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$r = $request->query;

		$limit = 10;
		$offset = !$r->has('page') ? 0 : $r->get('page') * $limit - $limit;

		$recipesRepo = $this->repo->getRepositoryOf('HealthyRecipe\HealthyRecipe');
		$recipes = $recipesRepo->findBy([], ['createdOn' => 'Desc'], $limit, $offset);

		$healthyPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'healthy'
		]);

		return $this->render('Pages/healthy.html.twig', [
			'recipes' => $recipes,
			'points' => [
				'healthy' => $healthyPage->getPoints(),
			]
		]);
	}

	/**
	 * @Route("/pages/healthy")
	 * @Method({"POST"})
	 */
	public function healthy2alAction(Request $request) {
		$recipePage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'healthy'
		]);

		if($request->files->has('healthy_bg')) {
			$background = $request->files->get('healthy_bg');
			if($background != null) {
				$path = $this->uploader->setFolder('healthy_main_bg');
				$bg = $path->upload($background);

				$request->request->set('healthy_bg', $bg);
			}
		}

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $recipePage->getPoint($k);

			if(!$point) continue;

			$point->setContent($i);
		}

		$this->em->persist($recipePage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/healthy');
	}

	/**
	 * @Route("/pages/feeds")
	 * @Method({"GET"})
	 */
	public function workou21tFeedsAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$feedRepo = $this->repo->getRepositoryOf('Workout\WorkOutFeed');
		$feeds = $feedRepo->findBy([], ['createdOn' => 'Desc'], 6);

		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		return $this->render('Pages/workout-feeds.html.twig', [
			'feeds' => $feeds,
			'points' => [
				'feed' => $feedPage->getPoints()
			]
		]);
	}

	/**
	 * @Route("/pages/feeds")
	 * @Method({"POST"})
	 */
	public function wofeed21CAction(Request $request) {
		/** @var $feedPage Page */
		$feedPage = $this->repo->getRepositoryOf('Page\Page')->findOneBy([
			'route' => 'wofeed'
		]);

		foreach($request->request->all() as $k => $i) {
			/** @var $point Point */
			$point = $feedPage->getPoint($k);

			if(!$point) {
				$point = $feedPage->getPoint($k);

				if(!$point) continue;
			}

			$point->setContent($i);
		}

		$this->em->persist($feedPage);
		$this->em->flush();

		return new RedirectResponse('/admin/pages/feeds');
	}
}
