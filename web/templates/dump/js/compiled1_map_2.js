$(document).ready(function() {
    $.get('/gdata', function(data) {
        $('#map-canvas').gmap3({
            marker:{
                options: {
                    icon:"templates/fleava_front/img/marker.png"
                },
                events: { // events trigged by markers
                    click: function(marker, event, context) {

                        if(data.type == 'mobile') {
                            window.location.href = context.data;
                        } else {
                            var map = $(this).gmap3("get"),
                                infowindow = $(this).gmap3({get:{name:"infowindow"}});
                            if (infowindow){
                                infowindow.open(map, marker);
                                infowindow.setContent(context.data);
                            } else {
                                $(this).gmap3({
                                    infowindow:{
                                        anchor:marker,
                                        options:{content: context.data}
                                    }
                                });
                            }
                        }
                    }
                },
                values: data.maps
            },
            map:{
                options:{
                    center: new google.maps.LatLng(26.3339896, 17.26921),
                    zoom: 3,
                    minZoom:3,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    panControl: false,
                    zoomControl: true,
                    scaleControl: true,
                    mapTypeControl:false,
                    disableDefaultUI: false,
                    styles: [
                        {
                            "featureType": "road.highway",
                            "stylers": [
                                { "visibility": "off" }
                            ]
                        },{
                            "stylers": [
                                { "saturation": -100 }
                            ]
                        }
                    ],
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.LEFT_BOTTOM
                    }
                }
            }
        });
    });

});