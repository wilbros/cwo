<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DinnerMenu
 * @package Chef\DomainBundle\Entity\ChefRecipe
 * @ORM\Entity
 */
class DinnerMenu extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $name;

	public function __construct($name) {
		parent::__construct();
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

} 