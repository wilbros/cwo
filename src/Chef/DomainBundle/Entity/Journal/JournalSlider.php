<?php

namespace Chef\DomainBundle\Entity\Journal;

use Chef\DomainBundle\Entity\Slider;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class JournalSlider extends Slider {

	/** @ORM\OneToOne(targetEntity="Journal") */
	private $reference;

	public function __construct(Journal $journal) {
		parent::__construct();

		$this->reference = $journal;

		$journal->setSlider($this);
	}

	public function getReference() {
		return $this->reference;
	}

	public function getName() {
		return 'Journal';
	}

	function getAdminBackLink() {
		return sprintf("/admin/%s/edit/%s", "journal", $this->reference->getId());
	}

	function getBackLink() {
		return sprintf("/%s/%s", "journals", $this->reference->getSlug());
	}
}