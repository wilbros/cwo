<?php

namespace Chef\DomainBundle\Entity;

interface SliderFlagInterface {
	function flagAsSlider();

	function removeAsSlider();

	function isSlider();
} 