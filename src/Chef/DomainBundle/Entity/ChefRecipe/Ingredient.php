<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Step
 * @package Chef\DomainBundle\Entity\ChefRecipe
 * @ORM\Entity
 */
class Ingredient extends AbstractEntity {

	/**
	 * @ORM\ManyToOne(targetEntity="Preparation", inversedBy="ingredients")
	 * @ORM\JoinColumn(name="preparation", referencedColumnName="id")
	 */
	private $preparation;

	/** @ORM\Column(type="text") */
	private $content;

	public function __construct(Preparation $preparation, $content) {
		parent::__construct();
		$this->preparation = $preparation;
		$this->content = $content;
	}

	/**
	 * @return mixed
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param mixed $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return mixed
	 */
	public function getPreparation()
	{
		return $this->preparation;
	}
} 