<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @package Chef\DomainBundle\Entity\ChefRecipe
 * @ORM\Entity
 */
class Tag extends AbstractEntity {

	/** @ORM\Column(type="string") */
	protected $name;

	/** @ORM\Column(type="integer") */
    protected $count = 0;

	public function __construct($name) {
		parent::__construct();

		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function increment() {
		++$this->count;

		return $this;
	}
} 