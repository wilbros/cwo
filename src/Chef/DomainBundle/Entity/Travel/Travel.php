<?php

namespace Chef\DomainBundle\Entity\Travel;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Travel extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $name;

	/** @ORM\Column(type="string") */
	private $description;

	/** @ORM\Column(type="string") */
	private $address;

	/** @ORM\Column(type="string") */
	private $featuredImage;

	/** @ORM\Column(type="string") */
	private $gmap_keyword;

	/** @ORM\Column(type="string") */
	private $website;

    /**
     * @ORM\ManyToOne(targetEntity="CategoryTravel")
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     */
    private $category;

	public function __construct($name, $description, $address, $featuredImage, $gmap_keyword, $website = '') {
		parent::__construct();

		$this->name = $name;

		$this->description = $description;

		$this->address = $address;

		$this->featuredImage = $featuredImage;

		$this->gmap_keyword = $gmap_keyword;

		$this->website = $website;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
	}

	public function getFeaturedImage() {
		return $this->featuredImage;
	}

	public function setFeaturedImage($featuredImage) {
		$this->featuredImage = $featuredImage;
	}

	public function getGmapKeyword() {
		return $this->gmap_keyword;
	}

	public function setGmapKeyword($gmap_keyword) {
		$this->gmap_keyword = $gmap_keyword;
	}

	public function getWebsite() {
		return $this->website;
	}

	public function setWebsite($website) {
		$this->website = $website;
	}

    public function getCategory() {
        return $this->category;
    }

    public function setCategory(CategoryTravel $category) {
        $this->category = $category;
    }
} 