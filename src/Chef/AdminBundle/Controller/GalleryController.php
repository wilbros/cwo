<?php

namespace Chef\AdminBundle\Controller;

use Chef\AdminBundle\Service\Uploader\Uploader;
use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\FoodGallery\CategoryGallery;
use Chef\DomainBundle\Entity\FoodGallery\FoodGallery;
use Chef\DomainBundle\Repository\Repository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class GalleryController extends BaseController {
	private $repo, $em, $uploader, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "uploader" = @JMS\Inject("uploader.local")
	 * })
	 */
	public function __construct(Repository $repo, Uploader $uploader, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->uploader = $uploader;

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

	/**
	 * @Route("/gallery")
	 * @Method({"GET"})
	 */
	public function galleryAllAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

		$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');
		$categories = $categoryRepo->findAll();

		$lists = [];

		foreach($categories as $category) {
			$lists[$category->getAlias()] = $galleryRepo->findBy(['category' => $category]);
		}

		$normalize = [];
		foreach($lists as $list) {
			foreach($list as $data) {
				$normalize[] = $data;
			}
		}

		return $this->render('gallery/all.html.twig', [
			'images' => $normalize,
			'categories' => $categories
		]);
	}

	/**
	 * @Route("/gallery/add")
	 * @Method({"GET"})
	 */
	public function galleryAddAction() {
		try{
			if(!$this->session->has(SESSION_KEY)) {
				return new RedirectResponse('/admin/login');
			}

			$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');
			$categories = $categoryRepo->findAll();

			return $this->render('gallery/add.html.twig', [
				'categories' => $categories
			]);
		} catch(\Exception $e) {
			var_dump($e->getMessage());exit;
		}
	}

	/**
	 * @Route("/gallery/add")
	 * @Method({"POST"})
	 */
	public function galleryPostAction(Request $request) {

		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$data = $request->request;

		$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');
		$category = $categoryRepo->find($data->get('categoryId'));

		$path = $this->uploader->setFolder('gallery');

		if($data->has('id')) {
			$galleryRepo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');
			/** @var $gallery FoodGallery */
			$gallery = $galleryRepo->find($data->get('id'));

			if($request->files->get('image')) {
				$path = $path->upload($request->files->get('image'));
				$gallery->setFeaturedImage($path);
			}

			$gallery->setTitle($data->get('title'));
			$gallery->setCategory($category);
		} else {
			$path = $path->upload($request->files->get('image'));
			$gallery = new FoodGallery($data->get('title'), $category, $path);
		}

		if($category->getId() == 3) {
			$gallery->setDescription($data->get('description'));
		} else {
			$gallery->setDescription('');
		}

		$this->em->persist($gallery);
		$this->em->flush();

		if($data->has('id')) {
			$id = $data->get('id');

			return $this->redirectWithFlash("/admin/gallery/edit/{$id}", $this->session, [
				'message' => [
					'code' => 200,
					'type' => 'update'
				]
			]);
		}

		return $this->redirectWithFlash($request->getRequestUri(), $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'insert'
			]
		]);
	}

	/**
	 * @Route("/gallery/edit/{id}")
	 * @Method({"GET"})
	 */
	public function galleryEditAction(Request $request, $id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		try {
			$categoryRepo = $this->repo->getRepositoryOf('FoodGallery\CategoryGallery');
			$categories = $categoryRepo->findAll();

			$repo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');

			$data = $repo->find($id);

			return $this->render('gallery/add.html.twig', [
				'categories' => $categories,
				'gallery' => $data
			]);
		} catch(\Exception $e) {
			var_dump($e->getMessage());exit;
		}
	}

    /**
     * @Route("/gallery/delete/{id}")
     * @Method({"GET"})
     */
    public function gallsderyDeleteAction(Request $request, $id) {
        if(!$this->session->has(SESSION_KEY)) {
            return new RedirectResponse('/admin/login');
        }

        $repo = $this->repo->getRepositoryOf('FoodGallery\FoodGallery');
        /** @var $data FoodGallery */
        $data = $repo->find($id);

        $this->em->remove($data);
        $this->em->flush();

        return $this->redirectWithFlash('/admin/gallery', $this->session, [
            'message' => [
                'code' => 200,
                'type' => 'delete'
            ]
        ]);
    }
}
