<?php

namespace Chef\DomainBundle\Entity\Journal;

use Chef\AdminBundle\Service\Sluggable;
use Chef\DomainBundle\Entity\AbstractEntity;
use Chef\DomainBundle\Entity\Slider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Journal
 * @package Chef\DomainBundle\Entity\Journal
 * @ORM\Entity
 */
class Journal extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $title;

	/** @ORM\Column(type="string") */
	private $slug;

	/** @ORM\Column(type="text") */
	private $content;

	/** @ORM\Column(type="text") */
	private $featuredImage;

	/** @ORM\OneToOne(targetEntity="JournalSlider") */
	private $slider;

    /**
     * @ORM\ManyToMany(targetEntity="JournalTag", cascade={"all"})
     * @ORM\JoinColumn(name="tags", referencedColumnName="id")
     */
    private $tags;

	public function __construct($title, $content, $image) {
		parent::__construct();

		$this->setTitle($title);
		$this->slug = Sluggable::slug($title);
		$this->content = $content;
		$this->featuredImage = $image;
        $this->tags = new ArrayCollection();
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;


	}

	public function getSlug() {
		return $this->slug;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function getFeaturedImage() {
		return $this->featuredImage;
	}

	public function setFeaturedImage($featuredImage) {
		$this->featuredImage = $featuredImage;
	}

	public function getSlider() {
		return $this->slider;
	}

	public function setSlider(Slider $slider) {
		$this->slider = $slider;

		return $this;
	}

	public function removeSlider() {
		$this->slider = null;

		return $this;
	}

    public function getTags() {
        return $this->tags;
    }

    public function clearTags(EntityManagerInterface $em) {
        foreach($this->tags as $tag) {
            $em->remove($tag);
        }

        $this->tags->clear();

        $em->flush();

        return $this;
    }

    public function addTag(JournalTag $tag) {
        $tag->increment();

        $this->tags->add($tag);

        return $this;
    }

    public function setTags($tags) {
        $this->tags = $tags;
    }
} 