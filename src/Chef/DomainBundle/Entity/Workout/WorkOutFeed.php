<?php

namespace Chef\DomainBundle\Entity\Workout;

use Chef\AdminBundle\Service\Sluggable;
use Chef\DomainBundle\Entity\AbstractEntity;
use Chef\DomainBundle\Entity\Slider;
use Chef\DomainBundle\Entity\SliderFlagInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class WorkOutFeed
 * @package Chef\DomainBundle\Entity\Workout
 * @ORM\Entity
 */
class WorkOutFeed extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $title;

	/** @ORM\Column(type="string") */
	private $slug;

	/** @ORM\Column(type="string") */
	private $content;

	/** @ORM\Column(type="string") */
	private $featuredImage;

	/** @ORM\OneToOne(targetEntity="FeedSlider") */
	private $slider;

	public function __construct($title, $content, $image) {
		parent::__construct();

		$this->setTitle($title);
		$this->slug = Sluggable::slug($title);

		$this->content = $content;

		$this->featuredImage = $image;
	}

	public function setTitle($title) {
		$this->title = $title;


	}

	public function getSlug() {
		return $this->slug;
	}

	public function setSlug($slug) {
		$this->slug = $slug;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function getFeaturedImage() {
		return $this->featuredImage;
	}

	public function setFeaturedImage($featuredImage) {
		$this->featuredImage = $featuredImage;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getSlider() {
		return $this->slider;
	}

	public function setSlider(Slider $slider) {
		$this->slider = $slider;

		return $this;
	}

	public function removeSlider() {
		$this->slider = null;

		return $this;
	}
}