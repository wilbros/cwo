<?php

namespace Chef\AdminBundle\Controller;

use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\User\Profile;
use Chef\DomainBundle\Entity\User\User;
use Chef\DomainBundle\Repository\Repository;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class LoginController extends BaseController {

	private $repo, $em, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository")
	 * })
	 */
	public function __construct(Repository $repo, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->session = $session;

		if(!$this->session->isStarted()) {
			$this->session->start();
		}

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

	/**
	 * @Route("/login")
	 * @Method({"GET"})
	 */
	public function loginAction() {
		return $this->render('login.html.twig');
	}

	/**
	 * @Route("/login")
	 * @Method({"POST"})
	 */
	public function loginPostAction(Request $request) {
		$repo = $this->repo->getRepositoryOf('User\User');

		$user = $repo->findOneBy([
			'username' => $request->request->get('username'),
			'password'=> md5($request->request->get('password'))
		]);

		if($user) {
			$this->session->set(SESSION_KEY, $this->session->getId());
		}

		$url = $user ? '/admin' : '/admin/login';

		return new RedirectResponse($url);
	}

	/**
	 * @Route("/logout")
	 */
	public function logoutAction() {
		$this->session->remove(SESSION_KEY);

		return new RedirectResponse('/admin/login');
	}
}
