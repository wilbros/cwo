<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Preparation
 * @package Chef\DomainBundle\Entity\ChefRecipe
 * @ORM\Entity
 */
class Preparation extends AbstractEntity {

	/**
	 * @ORM\ManyToOne(targetEntity="ChefRecipe", inversedBy="preparations")
	 * @ORM\JoinColumn(name="recipe", referencedColumnName="id")
	 */
	private $recipe;

	/** @ORM\Column(type="text") */
	private $preparation;

	/**
	 * @ORM\OneToMany(targetEntity="Ingredient", mappedBy="preparation", cascade={"all"})
	 */
	private $ingredients;

	/** @ORM\Column(type="text") */
	private $method;

	public function __construct(ChefRecipe $recipe, $preparation, $method) {
		parent::__construct();
		$this->recipe = $recipe;
		$this->preparation = $preparation;
		$this->ingredients = new ArrayCollection();
		$this->method = $method;
	}

	/**
	 * @return mixed
	 */
	public function getPreparation()
	{
		return $this->preparation;
	}

	/**
	 * @param mixed $preparation
	 */
	public function setPreparation($preparation)
	{
		$this->preparation = $preparation;
	}

	/**
	 * @return mixed
	 */
	public function getIngredients()
	{
		return $this->ingredients;
	}

	public function clearIngredient(EntityManagerInterface $em) {
		foreach($this->ingredients as $i) {
			$em->remove($i);
		}
		$this->ingredients->clear();

		$em->flush();

		return $this;
	}

	public function addIngredient(Ingredient $ingredient) {
		$this->ingredients->add($ingredient);

		return $this;
	}

	/**
	 * @param mixed $ingredients
	 */
	public function setIngredients($ingredients)
	{
		$this->ingredients = $ingredients;
	}

	/**
	 * @return mixed
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @param mixed $method
	 */
	public function setMethod($method)
	{
		$this->method = $method;
	}

	/**
	 * @return mixed
	 */
	public function getRecipe()
	{
		return $this->recipe;
	}
} 