<?php

namespace Chef\DomainBundle\Entity\HealthyRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class HealthyTag
 * @package Chef\DomainBundle\Entity\HealthyRecipe
 * @ORM\Entity
 */
class HealthyTag extends AbstractEntity {

	/** @ORM\Column(type="string") */
	protected $name;

	/** @ORM\Column(type="integer") */
    protected $count = 0;

	public function __construct($name) {
		parent::__construct();

		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function increment() {
		++$this->count;

		return $this;
	}
} 