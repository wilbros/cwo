<?php

namespace Chef\DomainBundle\Entity\User;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package Chef\DomainBundle\Entity\User
 * @ORM\Entity
 */
class Profile extends AbstractEntity {
	/** @ORM\Column(type="string", nullable=true) */
	private $photo;

	/** @ORM\Column(type="string") */
	private $name;

	/** @ORM\Column(type="string", nullable=true) */
	private $email;

	public function __construct($name) {
		parent::__construct();

		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function getPhoto() {
		return $this->photo;
	}

	public function setPhoto($photo) {
		$this->photo = $photo;
	}

} 