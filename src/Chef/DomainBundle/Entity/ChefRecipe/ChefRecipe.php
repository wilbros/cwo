<?php

namespace Chef\DomainBundle\Entity\ChefRecipe;

use Chef\AdminBundle\Service\Sluggable;
use Chef\DomainBundle\Entity\AbstractEntity;
use Chef\DomainBundle\Entity\Slider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ChefRecipe
 * @package Chef\DomainBundle\Entity\ChefRecipe
 * @ORM\Entity
 */
class ChefRecipe extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $title;

	/** @ORM\Column(type="string") */
	private $slug;

	/** @ORM\Column(type="text") */
	private $firstParagraph;

	/** @ORM\Column(type="text") */
	private $secondParagraph;

	/**
	 * @ORM\OneToOne(targetEntity="Presentation", cascade={"all"})
	 * @ORM\JoinColumn(name="presentation", referencedColumnName="id")
	 */
	private $presentation;

	/**
	 * @ORM\OneToMany(targetEntity="Preparation", mappedBy="recipe", cascade={"all"})
	 * @ORM\JoinColumn(name="preparations", referencedColumnName="id")
	 */
	private $preparations;

	/**
	 * @ORM\ManyToMany(targetEntity="DinnerMenu", cascade={"persist", "merge", "detach"})
	 * @ORM\JoinColumn(name="dinnerMenu", referencedColumnName="id")
	 */
	private $dinnerMenu;

	/**
	 * @ORM\ManyToMany(targetEntity="RecipeMenu", cascade={"persist", "merge", "detach"})
	 * @ORM\JoinColumn(name="recipeMenu", referencedColumnName="id")
	 */
	private $recipeMenu;

	/**
	 * @ORM\ManyToMany(targetEntity="Tag", cascade={"all"})
	 * @ORM\JoinColumn(name="tags", referencedColumnName="id")
	 */
	private $tags;

	/** @ORM\Column(type="text") */
	private $featuredImage;

	/** @ORM\OneToOne(targetEntity="ChefSliderTrx") */
	private $slider;

	/** @ORM\Column(type="text") */
	private $content;

	public function __construct($title, $firstParagraph, $secondParagraph, $presentation, $featuredImage) {
		parent::__construct();
		$this->setTitle($title);
		$this->slug = Sluggable::slug($this->title);
		$this->firstParagraph = $firstParagraph;
		$this->secondParagraph = $secondParagraph;
		$this->presentation = $presentation;

		$this->featuredImage = $featuredImage;

		$this->preparations = new ArrayCollection();
		$this->dinnerMenu = new ArrayCollection();
		$this->recipeMenu = new ArrayCollection();
		$this->tags = new ArrayCollection();
		$this->content = $firstParagraph;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function getFirstParagraph() {
		return $this->firstParagraph;
	}

	public function setFirstParagraph($firstParagraph) {
		$this->firstParagraph = $firstParagraph;

		$this->content = $firstParagraph;
	}

	public function getSecondParagraph() {
		return $this->secondParagraph;
	}

	public function setSecondParagraph($secondParagraph) {
		$this->secondParagraph = $secondParagraph;
	}

	public function getPresentation() {
		return $this->presentation;
	}

	public function setPresentation(Presentation $presentation) {
		$this->presentation = $presentation;

		return $this;
	}

	public function getPreparations() {
		return $this->preparations;
	}

	public function removePreparation(Preparation $preparation, EntityManagerInterface $em) {
		return $this->preparations->map(function(Preparation $ptx) use($preparation, $em) {
			if($ptx->getId() == $preparation->getId()) {
				$em->remove($ptx);
				$this->preparations->removeElement($ptx);

				return true;
			}
		});
	}

	public function clearPreparations(EntityManagerInterface $em) {
		foreach($this->preparations as $p) {
			$em->remove($p);
		}
		$this->preparations->clear();

		$em->flush();

		return $this;
	}

	public function addPreparation(Preparation $preparation) {
		$this->preparations->add($preparation);

		return $this;
	}

	public function setPreparations($preparations) {
		$this->preparations = $preparations;
	}

	public function getDinnerMenu() {
		return $this->dinnerMenu;
	}

	public function clearDinnerMenu(EntityManagerInterface $em) {
		$this->dinnerMenu->clear();

		return $this;
	}

	public function addDinnerMenu(DinnerMenu $dinnerMenu) {
		$this->dinnerMenu->add($dinnerMenu);

		return $this;
	}

	public function getRecipeMenu() {
		return $this->recipeMenu;
	}

	public function clearRecipeMenu(EntityManagerInterface $em) {
		$this->recipeMenu->clear();

		return $this;
	}

	public function addRecipeMenu(RecipeMenu $recipeMenu) {
		$this->recipeMenu->add($recipeMenu);

		return $this;
	}

	public function getTags() {
		return $this->tags;
	}

	public function clearTags(EntityManagerInterface $em) {
		foreach($this->tags as $tag) {
			$em->remove($tag);
		}

		$this->tags->clear();

		$em->flush();

		return $this;
	}

	public function addTag(Tag $tag) {
		$tag->increment();

		$this->tags->add($tag);

		return $this;
	}

	public function setTags($tags) {
		$this->tags = $tags;
	}

	public function getFeaturedImage() {
		return $this->featuredImage;
	}

	public function setFeaturedImage($featuredImage) {
		$this->featuredImage = $featuredImage;
	}

	public function getSlider() {
		return $this->slider;
	}

	public function setSlider(Slider $slider) {
		$this->slider = $slider;

		return $this;
	}

	public function removeSlider() {
		$this->slider = null;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param mixed $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return mixed
	 */
	public function getSlug()
	{
		return $this->slug;
	}
} 