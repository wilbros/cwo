<?php

namespace Chef\AdminBundle\Controller;

use Chef\AdminBundle\Service\Uploader\Uploader;
use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\Journal\Journal;
use Chef\DomainBundle\Entity\Journal\JournalSlider;
use Chef\DomainBundle\Entity\Journal\JournalTag;
use Chef\DomainBundle\Entity\Slider;
use Chef\DomainBundle\Entity\Workout\WorkOutFeed;
use Chef\DomainBundle\Repository\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class JournalController extends BaseController {
	private $repo, $em, $uploader, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "uploader" = @JMS\Inject("uploader.local")
	 * })
	 */
	public function __construct(Repository $repo, Uploader $uploader, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->uploader = $uploader;

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

	/**
	 * @Route("/journal")
	 * @Method({"GET"})
	 */
	public function journalAllAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Journal\Journal');

		$data = $repo->findAll();

		return $this->render('journal/all.html.twig', [
			'journals' => $data
		]);
	}

	/**
	 * @Route("/journal/add")
	 * @Method({"GET"})
	 */
	public function journalAddAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		return $this->render('journal/add.html.twig');
	}

	/**
	 * @Route("/journal/add")
	 * @Method({"POST"})
	 */
	public function journalPostAction(Request $request) {
		try {
            if(!$this->session->has(SESSION_KEY)) {
                return new RedirectResponse('/admin/login');
            }

            $data = $request->request;

            $uploader = $path = $this->uploader->setFolder('journal');

            if($data->has('id')) {
                $repo = $this->repo->getRepositoryOf('Journal\Journal');

                /** @var $feed Journal */
                $feed = $repo->find($data->get('id'));

                if($request->files->get('image') !== null) {
                    $path = $uploader->upload($request->files->get('image'));
                    $feed->setFeaturedImage($path);
                }

                $feed->setTitle($data->get('title'));
                $feed->setContent($data->get('content'));

                $tags = explode(',', $data->get('tags'));

                $feed->clearTags($this->em);
                foreach($tags as $tag) {
                    $tagRepo = $this->repo->getRepositoryOf('Journal\JournalTag');
                    $tagTemp = $tagRepo->findOneBy(['name' => $tag]);

                    if(!$tagTemp) {
                        $tag = new JournalTag($tag);
                    } else {
                        $tag = $tagTemp;
                    }

                    $feed->addTag($tag);
                }
            } else {
                $path = $uploader->upload($request->files->get('image'));
                $feed = new Journal($data->get('title'), $data->get('content'), $path);

                $tags = explode(',', $data->get('tags'));
                foreach($tags as $tag) {
                    $tagRepo = $this->repo->getRepositoryOf('Journal\JournalTag');
                    $tagTemp = $tagRepo->findOneBy(['name' => $tag]);

                    if(!$tagTemp) {
                        $tag = new JournalTag($tag);
                    } else {
                        $tag = $tagTemp;
                    }

                    $feed->addTag($tag);
                }
            }

            $this->em->persist($feed);

            if($data->has('isSlider')) {
                $slider = $this->findSliderBelongTo($feed);

                if($slider === false) {
                    $slider = new JournalSlider($feed);
                    $this->em->persist($slider);
                }
            } else {
                $slider = $this->findSliderBelongTo($feed);

                if($slider !== false) {
                    $feed->removeSlider();

                    $this->em->remove($slider);
                }
            }

            $this->em->flush();

            if($data->has('id')) {
                $id = $data->get('id');

                return $this->redirectWithFlash("/admin/journal/edit/{$id}", $this->session, [
                    'message' => [
                        'code' => 200,
                        'type' => 'update'
                    ]
                ]);
            }

            return $this->redirectWithFlash($request->getRequestUri(), $this->session, [
                'message' => [
                    'code' => 200,
                    'type' => 'insert'
                ]
            ]);
        }catch (\Exception $e) {
            var_dump($e->getMessage());exit;
        }
	}

	public function findSliderBelongTo(Journal $feed) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Slider');
		$sliders = $repo->findAll();

		$collection = new ArrayCollection($sliders);
		$slider = $collection->filter(function(Slider $slider) use($feed) {
			return ($slider instanceof JournalSlider)
			and ($slider->getReference()->getId() === $feed->getId());
		})->first();

		return $slider;
	}

	/**
	 * @Route("/journal/edit/{id}")
	 * @Method({"GET"})
	 */
	public function journalEditAction(Request $request, $id) {
		try {
            if(!$this->session->has(SESSION_KEY)) {
                return new RedirectResponse('/admin/login');
            }

            $repo = $this->repo->getRepositoryOf('Journal\Journal');

            $data = $repo->find($id);

            return $this->render('journal/add.html.twig', [
                'journal' => $data
            ]);
        } catch(\Exception $e) {
            var_dump($e->getMessage());exit;
        }
	}

	/**
	 * @Route("/journal/delete/{id}")
	 * @Method({"GET"})
	 */
	public function journalDeleteAction(Request $request, $id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Journal\Journal');
		/** @var $data Journal */
		$data = $repo->find($id);

		if($data->getSlider() != null) {
			$slider = $data->getSlider();

			$this->em->remove($slider);
		}

		$this->em->remove($data);
		$this->em->flush();

		return $this->redirectWithFlash('/admin/journal', $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'delete'
			]
		]);
	}
}
