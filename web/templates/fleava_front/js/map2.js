// generate an array of colors
//var colors = "Chef Friends-Restaurant Reviews-Sport Competitions".split("-");

$.get('/gdata/markers',  function(c) {
  var colors = c.markers;

  $(function(){
    // create colors checkbox and associate onChange function
    $.each(colors, function(i, color){
      name = color;
      color = color.replace(" ", "-");
      color = color.toLowerCase();
      $("#colors ul.dropdown-menu").append("<li><a><input type='checkbox' name='"+color+"' id='"+color+"' checked> &nbsp; <label for='"+color+"'>"+name+"</label>  &nbsp;  &nbsp; </a><span class='"+color+"'></span></li>");
    });

    $("#colors input[type=checkbox]").change(onChangeChk);
    $("#onOff").change(onChangeOnOff);

    $('#colors .dropdown-toggle').on('click', function (event) {
      $(this).parent().toggleClass("open");
    });

    $('body').on('click', function (e) {
      if (!$('#colors .dropdown-toggle').is(e.target) && $('li.dropdown.mega-dropdown').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
        $('#colors .dropdown-toggle').removeClass('open');
      }
    });


    // create gmap3 and call the marker generation function
    $('#map-canvas').gmap3({
      map:{
        options:{
          center: new google.maps.LatLng(26.3339896, 17.26921),
          zoom: 3,
          minZoom:3,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          panControl: false,
          zoomControl: true,
          scaleControl: true,
          mapTypeControl:false,
          disableDefaultUI: false,
          styles: [
            {
              "featureType": "road.highway",
              "stylers": [
                { "visibility": "off" }
              ]
            },{
              "stylers": [
                { "saturation": -100 }
              ]
            }
          ]
        },
        onces: {
          bounds_changed: function() { createMarker() }
        }
      }
    });

  });


  function createMarker() {
    $.get('/gdata',  function(data) {
      $('#map-canvas').gmap3({
        marker:{
          cluster:{
            radius: 1,
            // This style will be used for clusters with more than 0 markers
            0: {
              content: '<div class="cluster cluster-1">CLUSTER_COUNT</div>',
              width: 53,
              height: 52
            },
            // This style will be used for clusters with more than 20 markers
            20: {
              content: '<div class="cluster cluster-2">CLUSTER_COUNT</div>',
              width: 56,
              height: 55
            },
            // This style will be used for clusters with more than 50 markers
            50: {
              content: '<div class="cluster cluster-3">CLUSTER_COUNT</div>',
              width: 66,
              height: 65
            }
          },
          events: { // events trigged by markers
            click: function(marker, event, context) {

              if(data.type == 'mobile') {
                window.location.href = context.data;
              } else {
                var map = $(this).gmap3("get"),
                    infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                  infowindow.open(map, marker);
                  infowindow.setContent(context.data);
                } else {
                  $(this).gmap3({
                    infowindow:{
                      anchor:marker,
                      options:{content: context.data}
                    }
                  });
                }
              }
            }
          },
          values: data.maps
        }
      });
    });
  }

  function onChangeOnOff(){
    if ($(this).is(":checked")){
      $('#map-canvas').gmap3({get:"clusterer"}).enable();
    } else {
      $('#map-canvas').gmap3({get:"clusterer"}).disable();
    }
  }

  function onChangeChk(){
    // first : create an object where keys are colors and values is true (only for checked objects)
    var checkedColors = {};
    $("#colors input[type=checkbox]:checked").each(function(i, chk){
      checkedColors[$(chk).attr("name")] = true;
    });

    // set a filter function using the closure data "checkedColors"
    $('#map-canvas').gmap3({get:"clusterer"}).filter(function(data){
      return data.tag in checkedColors;
    });
  }
});
