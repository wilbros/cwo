<?php

namespace Chef\DomainBundle\Entity\User;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package Chef\DomainBundle\Entity\User
 * @ORM\Entity
 */
class User extends AbstractEntity {
	/** @ORM\Column(type="string") */
	private $username;

	/** @ORM\Column(type="string") */
	private $password;

	/** @ORM\OneToOne(targetEntity="Profile", cascade={"all"}) */
	private $profile;

	public function __construct($username, $password, Profile $profile) {
		parent::__construct();

		$this->username = $username;

		$this->password = $password;

		$this->profile = $profile;
	}

	public function getUsername() {
		return $this->username;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

	public function getProfile() {
		return $this->profile;
	}

	public function setProfile(Profile $profile) {
		$this->profile = $profile;

		return $this;
	}
} 