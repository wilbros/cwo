<?php

namespace Chef\AdminBundle\Controller;

use Chef\AdminBundle\Service\Uploader\Uploader;
use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\Journal\Journal;
use Chef\DomainBundle\Entity\Journal\JournalSlider;
use Chef\DomainBundle\Entity\Slider;
use Chef\DomainBundle\Entity\Workout\WorkOutFeed;
use Chef\DomainBundle\Repository\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class SliderController extends BaseController {
	private $repo, $em, $uploader, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "uploader" = @JMS\Inject("uploader.local")
	 * })
	 */
	public function __construct(Repository $repo, Uploader $uploader, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->uploader = $uploader;

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

	/**
	 * @Route("/slider")
	 * @Method({"GET"})
	 */
	public function sliderAllAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Slider');

		$data = $repo->findAll();

		return $this->render('Slider/all.html.twig', [
			'sliders' => $data
		]);
	}
}
