<?php

namespace Chef\DomainBundle\Entity\Workout;

use Chef\DomainBundle\Entity\Slider;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class FeedSlider extends Slider {

	/** @ORM\OneToOne(targetEntity="WorkOutFeed") */
	private $reference;

	public function __construct(WorkOutFeed $feed) {
		parent::__construct();

		$this->reference = $feed;

		$feed->setSlider($this);
	}

	public function getReference() {
		return $this->reference;
	}

	public function getName() {
		return 'Work Out Feed';
	}

	public function getAdminBackLink() {
		return sprintf("/admin/%s/edit/%s", "workout-feeds", $this->reference->getId());
	}

	function getBackLink() {
		return "#";
	}
}