<?php

namespace Chef\DomainBundle\Entity\Journal;


use Chef\DomainBundle\Entity\AbstractEntity;
use Chef\DomainBundle\Entity\ChefRecipe\Tag;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class JournalTag
 * @package Chef\DomainBundle\Entity\Journal
 * @ORM\Entity
 */
class JournalTag extends AbstractEntity {

    /** @ORM\Column(type="string") */
    protected $name;

    /** @ORM\Column(type="integer") */
    protected $count = 0;

    public function __construct($name) {
        parent::__construct();

        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function increment() {
        ++$this->count;

        return $this;
    }
} 