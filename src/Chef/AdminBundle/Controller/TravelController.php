<?php

namespace Chef\AdminBundle\Controller;

use Chef\AdminBundle\Service\Uploader\Uploader;
use Chef\DomainBundle\Entity\Connect\Connect;
use Chef\DomainBundle\Entity\Travel\CategoryTravel;
use Chef\DomainBundle\Entity\Travel\Travel;
use Chef\DomainBundle\Repository\Repository;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/admin")
 */
class TravelController extends BaseController {
	private $repo, $em, $uploader, $session;

	/**
	 * @JMS\InjectParams({
	 *      "repo" = @JMS\Inject("repository"),
	 *      "uploader" = @JMS\Inject("uploader.local")
	 * })
	 */
	public function __construct(Repository $repo, Uploader $uploader, Session $session) {
		$this->repo = $repo;
		$this->em = $repo->getManager();
		$this->uploader = $uploader;

		$this->session = $session;

		//auto generating mode on
		//$this->repo->refreshSchema();
	}

	/**
	 * @Route("/travel")
	 * @Method({"GET"})
	 */
	public function travelAllAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Travel\Travel');

		return $this->render('travel/all.html.twig', [
			'travels' => $repo->findAll()
		]);
	}

	/**
	 * @Route("/travel/add")
	 * @Method({"GET"})
	 */
	public function travelAction() {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

        $repo = $this->repo->getRepositoryOf('Travel\CategoryTravel');

		return $this->render('travel/add.html.twig', [
            'categories' => $repo->findBy(['removedOn' => null])
        ]);
	}

	/**
	 * @Route("/travel/add")
	 * @Method({"POST"})
	 */
	public function travelPostAction(Request $request) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$data = $request->request;

		$uploader = $this->uploader->setFolder('travel');

		if($data->has('id')) {
			$repo = $this->repo->getRepositoryOf('Travel\Travel');

			/** @var $travel Travel */
			$travel = $repo->find($data->get('id'));

			if($request->files->get('image') !== null) {
				$path = $uploader->upload($request->files->get('image'));
				$travel->setFeaturedImage($path);
			}

			$travel->setName($data->get('name'));
			$travel->setWebsite($data->get('website'));
			$travel->setDescription($data->get('content'));
			$travel->setAddress($data->get('address'));
			$travel->setGmapKeyword($data->get('gmap_keyword'));
		} else {
			$path = $uploader->upload($request->files->get('image'));
			$travel = new Travel($data->get('name'), $data->get('content'), $data->get('address'), $path, $data->get('gmap_keyword'), $data->get('website'));
		}

        $categoryRepo = $this->repo->getRepositoryOf('Travel\CategoryTravel');
        $category = $categoryRepo->find($data->get('categoryId'));

        $travel->setCategory($category);

		$this->em->persist($travel);
		$this->em->flush();

		if($data->has('id')) {
			$id = $data->get('id');

			return $this->redirectWithFlash("/admin/travel/edit/{$id}", $this->session, [
				'message' => [
					'code' => 200,
					'type' => 'update'
				]
			]);
		}

		return $this->redirectWithFlash('/admin/travel/add', $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'insert'
			]
		]);
	}

	/**
	 * @Route("/travel/edit/{id}")
	 * @Method({"GET"})
	 */
	public function travelEditAction(Request $request, $id) {
        try{
            if(!$this->session->has(SESSION_KEY)) {
                return new RedirectResponse('/admin/login');
            }

            $repo = $this->repo->getRepositoryOf('Travel\Travel');

            $data = $repo->find($id);

            $repoCtg = $this->repo->getRepositoryOf('Travel\CategoryTravel');
            return $this->render('travel/add.html.twig', [
                'travel' => $data,
                'categories' => $repoCtg->findBy(['removedOn' => null])
            ]);
        } catch(\Exception $e) {
            var_dump($e->getMessage());exit;
        }
	}

	/**
	 * @Route("/travel/delete/{id}")
	 * @Method({"GET"})
	 */
	public function travelDeleteAction(Request $request, $id) {
		if(!$this->session->has(SESSION_KEY)) {
			return new RedirectResponse('/admin/login');
		}

		$repo = $this->repo->getRepositoryOf('Travel\Travel');
		/** @var $data Travel */
		$data = $repo->find($id);

		$this->em->remove($data);
		$this->em->flush();

		return $this->redirectWithFlash('/admin/travel', $this->session, [
			'message' => [
				'code' => 200,
				'type' => 'delete'
			]
		]);
	}
}
