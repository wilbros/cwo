<?php

namespace Chef\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *      "slider"="Slider",
 *      "chefSliderTrx"="Chef\DomainBundle\Entity\ChefRecipe\ChefSliderTrx",
 *      "healthySlider"="Chef\DomainBundle\Entity\HealthyRecipe\HealthySlider",
 *      "feedSlider"="Chef\DomainBundle\Entity\Workout\FeedSlider",
 *      "journalSlider"="Chef\DomainBundle\Entity\Journal\JournalSlider"
 * })
 */
abstract class Slider extends AbstractEntity {

	public function __construct() {
		parent::__construct();
	}

	abstract function getName();

	abstract function getAdminBackLink();

	abstract function getBackLink();
}