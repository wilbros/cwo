<?php

namespace Chef\DomainBundle\Entity\Connect;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Connect
 * @package Chef\DomainBundle\Entity\Connect
 * @ORM\Entity
 */
class ConnectCompany extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $title;

	/** @ORM\Column(type="string") */
	private $company;

	/** @ORM\Column(type="text") */
	private $address;

	/** @ORM\Column(type="string") */
	private $phone;

	/** @ORM\Column(type="string") */
	private $fax;

	/** @ORM\Column(type="array") */
	private $email;

	public function __construct() {
		parent::__construct();

		$this->title = '';
		$this->company = '';
		$this->address = '';
		$this->phone = '';
		$this->fax = '';
		$this->email = [];
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return mixed
	 */
	public function getCompany()
	{
		return $this->company;
	}

	/**
	 * @param mixed $company
	 */
	public function setCompany($company)
	{
		$this->company = $company;
	}

	/**
	 * @return mixed
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param mixed $address
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	}

	/**
	 * @return mixed
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param mixed $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @return mixed
	 */
	public function getFax()
	{
		return $this->fax;
	}

	/**
	 * @param mixed $fax
	 */
	public function setFax($fax)
	{
		$this->fax = $fax;
	}

	public function clearEmail() {
		$this->email = [];

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	public function addEmail($email) {
		$this->email[] = $email;

		return $this;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}


} 