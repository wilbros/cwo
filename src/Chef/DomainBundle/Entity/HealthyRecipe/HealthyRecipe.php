<?php

namespace Chef\DomainBundle\Entity\HealthyRecipe;

use Chef\AdminBundle\Service\Sluggable;
use Chef\DomainBundle\Entity\AbstractEntity;
use Chef\DomainBundle\Entity\Slider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class HealthyRecipe
 * @package Chef\DomainBundle\Entity\Journal
 * @ORM\Entity
 */
class HealthyRecipe extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $title;

	/** @ORM\Column(type="string") */
	private $slug;

	/** @ORM\Column(type="text") */
	private $content;

	/** @ORM\Column(type="text") */
	private $featuredImage;

	/** @ORM\OneToOne(targetEntity="HealthySlider") */
	private $slider;

    /**
     * @ORM\ManyToMany(targetEntity="HealthyTag", cascade={"all"})
     * @ORM\JoinColumn(name="tags", referencedColumnName="id")
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity="DinnerMenuHealthy", cascade={"persist", "merge", "detach"})
     * @ORM\JoinColumn(name="dinnerMenu", referencedColumnName="id")
     */
    private $dinnerMenu;

    /**
     * @ORM\ManyToMany(targetEntity="RecipeMenuHealthy", cascade={"persist", "merge", "detach"})
     * @ORM\JoinColumn(name="recipeMenu", referencedColumnName="id")
     */
    private $recipeMenu;

	public function __construct($title, $content, $image) {
		parent::__construct();

		$this->setTitle($title);
		$this->slug = Sluggable::slug($title);
		$this->content = $content;
		$this->featuredImage = $image;
        $this->tags = new ArrayCollection();
        $this->dinnerMenu = new ArrayCollection();
        $this->recipeMenu = new ArrayCollection();
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function getSlug() {
		return $this->slug;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function getFeaturedImage() {
		return $this->featuredImage;
	}

	public function setFeaturedImage($featuredImage) {
		$this->featuredImage = $featuredImage;
	}

	public function getSlider() {
		return $this->slider;
	}

	public function setSlider(Slider $slider) {
		$this->slider = $slider;

		return $this;
	}

	public function removeSlider() {
		$this->slider = null;

		return $this;
	}

    public function getTags() {
        return $this->tags;
    }

    public function clearTags(EntityManagerInterface $em) {
        foreach($this->tags as $tag) {
            $em->remove($tag);
        }

        $this->tags->clear();

        $em->flush();

        return $this;
    }

    public function addTag(HealthyTag $tag) {
        $tag->increment();

        $this->tags->add($tag);

        return $this;
    }

    public function setTags($tags) {
        $this->tags = $tags;
    }

    public function getDinnerMenu() {
        return $this->dinnerMenu;
    }

    public function clearDinnerMenu(EntityManagerInterface $em) {
        $this->dinnerMenu->clear();

        return $this;
    }

    public function addDinnerMenu(DinnerMenuHealthy $dinnerMenu) {
        $this->dinnerMenu->add($dinnerMenu);

        return $this;
    }

    public function getRecipeMenu() {
        return $this->recipeMenu;
    }

    public function clearRecipeMenu(EntityManagerInterface $em) {
        $this->recipeMenu->clear();

        return $this;
    }

    public function addRecipeMenu(RecipeMenuHealthy $recipeMenu) {
        $this->recipeMenu->add($recipeMenu);

        return $this;
    }

} 