<?php

namespace Chef\DomainBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
Use JMS\DiExtraBundle\Annotation as JMS;

const __DOMAIN__ = 'Chef\\DomainBundle\\Entity\\';

/** @JMS\Service("repository") */
class Repository {
	private $em;

	/**
	 * @JMS\InjectParams({
	 *      "em" = @JMS\Inject("doctrine.orm.entity_manager")
	 * })
	 */
	public function __construct(EntityManager $em) {
		$this->em = $em;
	}

	public function getRepositoryOf($entity) {
		if(!is_string($entity)) {
			throw new \Exception('Entity must be a String, '.gettype($entity).' given!');
		}

		try {
			//$this->refreshSchema();

			return $this->em->getRepository(__DOMAIN__.$entity);
		} catch(\Exception $e) {
			throw new \Exception($e->getMessage());
		}

		return false;
	}

	public function getManager() {
		return $this->em;
	}

	public function refreshSchema() {
		$tool = new SchemaTool($this->em);
		$tool->updateSchema(
			$this->em->getMetadataFactory()->getAllMetadata(),
			true
		);
	}
} 