<?php

namespace Chef\AdminBundle\Controller;

class DummyController {
	function a() {
		$hi = new Page('wohighintensity');
		$hi->addPoint(new Point($hi, 'wohi_title', '“…TRAIN HARDER, BUT TRAIN BRIEFER”'));
		$hi->addPoint(new Point($hi, 'wohi_content', 'Concentrated during your work out and push yourself to the limit, it helps to improve power and strengthen muscles. The harder or more intense an exercise is, the greater the degree of overload and the more effective the exercise. There are many way to execute an HIT, with different exercises, number of repetitions and speed but the most important during the exercise is to be concentrate on the perfect execution of the movements and stay motivated, because the goal is working hard as much as possible and not giving up!'));
		$hi->addPoint(new Point($hi, 'wohi_bg1', 'http://chefworkout.dev/templates/fleava_front/img/workout1.jpg'));
		$hi->addPoint(new Point($hi, 'wohi_md1', 'One way to increase the power and muscle mass is to increase the weight by 5% every week, another is the overload at the end of each exercise with an extra 2-3 forced repetitions until it’s impossible to perform another repetition in good form. A training session generally require around 30 to 45 minutes. HIT is not only for bodybuilders, it can easily be associated with cardio or body weight exercises ,the most important thing is to train all specific muscles group, in the right way. Most of the people who go to the gym, focus their training on the “cool” muscles like chest, biceps, while ignoring the rest. It’s always good to start a training with a large muscles exercises (chest, back or legs) to stimulate the other smaller (push-ups are mainly a chest exercise which also stimulate shoulders and triceps) then continue on training with isolated muscles exercises (upright barbell rows, bench dips...)'));
		$hi->addPoint(new Point($hi, 'wohi_md2', 'The appropriate volume of exercises varies significantly between individuals based on genetics, age, and lifestyle factors, such as quality and amount of nutrition and rest, as well as the specific training goals. Obviously beginners are not recommended to do this kind of training because of the significant amount of stress on the body. It is very important to build a solid base before and to be able to push yourself to the limit , or the results will not be satisfying. Even for people with more experience it is very important to use this training program in the correct way. One of the few rules to follow is to train often but to always let the body recover in between workouts.'));
		$hi->addPoint(new Point($hi, 'wohi_low_title', 'High Intensity Training (HIT)'));
		$hi->addPoint(new Point($hi, 'wohi_low_content', 'The HIT can easily be modified and adapted for any individual, the most important thing is following the general guidelines and keeping in mind what is the final goal is.'));
		$hi->addPoint(new Point($hi, 'wohi_low_bg', 'http://chefworkout.dev/templates/fleava_front/img/hero12.jpg'));

		$hi->addPoint(new Point($hi, 'wohi_md_title1', 'Many studies has been done by scientists that prove the efficacy of HIT. These studies also highlight how aerobic activities contribute to losing weight but do not built muscles. On the other hand HIT burns fat! Some aerobic activities are highly effective for improving cardiovascular and metabolic conditioning but at the same time can also increase the power of the muscles.

Once you’ve decided what the target is (losing weight, building muscles, definition) we need to start focusing on the training: The most important thing to remember? UNDERSTAND YOUR BODY!!!'));

		$hi->addPoint(new Point($hi, 'wohi_md_title2', 'This is more or less what all personal trainers do, helping people reach their health and fitness goals, by understanding what kind of body they are training. Everyone has different body shapes, motivations, avaiability and HIT can suit to any of these needs.

For example for a thin person I would never suggest cardio activity during his training session, instead the HIT will focus on weight and body weight exercises addressing all major muscle groups, not too high number of repletion of each exercise and every 2 to 3 week increase 5% on the weight.'));

		$hi->addPoint(new Point($hi, 'wohi_poster_title', 'Try to follow these few simple guide lines before planning your training:'));
		$hi->addPoint(new Point($hi, 'wohi_poster_content', ' <p class="text-white bold lead">Train all parts of your body; neglecting other important range of muscles the result will be incomplete.</p>
                                <p class="text-white bold lead">No consecutive days or more than three workouts per week. Perform between two and ten exercises addressing all major muscle groups. </p>
                                <p class="text-white bold lead">It’s essential to concentrate on progression and increase the volume of your trainings, always try to give the best you can until temporary muscles failure.</p>
                                <p class="text-white bold lead">Slow movements, maintain strict control of the posture. Avoid fast repetitions and swinging during the exercise. </p>'));
		$hi->addPoint(new Point($hi, 'wohi_poster_bg', 'http://chefworkout.dev/templates/fleava_front/img/hero10.jpg'));


		$hi->addPoint(new Point($hi, 'wohi_doll_title', 'This is an example of a weekly HIT workout beginner to improve mainly upper muscles (2 times a week).'));

		$hi->addPoint(new Point($hi, 'wohi_tanggungjawab', 'For each exercise you should be able to do between 10-15 repetitions for 3 sets. 30 second rest, 3 minutes in between each set'));

		$hi->addPoint(new Point($hi, 'wohi_title1', 'This is an example of a weekly HIT workout beginner to improve mainly upper muscles (2 times a week).'));

		$hi->addPoint(new Point($hi, 'wohi_title_exer1', '1ST DAY'));

		$hi->addPoint(new Point($hi, 'wohi_content_exer1', '<li>1. Pull down</li>
                                        <li>2. Squat (only body weight)</li>
                                        <li>3. Pullover</li>
                                        <li>4. Leg Extension</li>
                                        <li>5. Leg curl</li>
                                        <li>6. Barbell Curl</li>
                                        <li>7. Back extensions</li>'));

		$hi->addPoint(new Point($hi, 'wohi_title_exer2', '2ND DAY'));

		$hi->addPoint(new Point($hi, 'wohi_content_exer2', '<li>1. Pushups or dip</li>
                                        <li>2. Shoulder Press  </li>
                                        <li>3. Chest fly</li>
                                        <li>4. Lateral raise</li>
                                        <li>5.  Shrug</li>
                                        <li>6. Triceps extension</li>
                                        <li>7. Abs Crunches</li>'));

		$hi->addPoint(new Point($hi, 'wohi_title_foot1', 'Introduction to Workout'));
		$hi->addPoint(new Point($hi, 'wohi_content_foot1', 'The HIT can easily be modified and adapted for any individual, the most important thing is following the general guidelines and keeping in mind what is the final goal is.'));
		$hi->addPoint(new Point($hi, 'wohi_bg_foot1', 'http://chefworkout.dev/templates/fleava_front/img/workout4.jpg'));

		$hi->addPoint(new Point($hi, 'wohi_title_foot2', 'Fitness & Diet'));
		$hi->addPoint(new Point($hi, 'wohi_content_foot2', 'How many times you probably heard people complaining about their body with words like: “…I did so many abs but I still have my belly fat!” or “…I’m sweating a lot but I’m not losing weight”'));
		$hi->addPoint(new Point($hi, 'wohi_bg_foot2', 'http://chefworkout.dev/templates/fleava_front/img/workout3.jpg'));

		$this->em->persist($hi);
		$this->em->flush();
	}

	function b() {
		$hi = new Page('wofdfitness');

		//1
		$hi->addPoint(new Point($hi, 'wofd_title', 'NUTRITION AFTER WORK OUT'));
		$hi->addPoint(new Point($hi, 'wofd_subtitle', 'How many times you probably heard people complaining about their body with words like: “…I did so many abs but I still have my belly fat!” or “…I’m sweating a lot but I’m not losing weight”?'));
		$hi->addPoint(new Point($hi, 'wofd_content1', 'The correct answer once again is “a balanced lifestyle”, I love challenging myself with variety of different workouts, running, swimming but even the perfect workout not giving enough if after a training section the recovery and most important the diet is wrong. Not giving enough nourishment to our muscles after work-out is damaging our metabolism. The only way for our body to produce energy is from our muscles (Gluconeogenesis)<br><br>The result will be muscular tone loss and the body will preserve the fat for food, that’s why feeding our muscles after the training with an appropriate nourishment is very important.'));
		$hi->addPoint(new Point($hi, 'wofd_content2', 'Everyday day our muscles need a certain amount of energy and this energy comes from the food we eat, a person who does sport activities needs to provide the body with enough nutrients to meet the demands of training and have a proper recovery between exercise sessions. HIT generally increases these needs depending on the type of activity and the intensity.'));
		$hi->addPoint(new Point($hi, 'wofd_content3', 'The three main nutrients from food that supply the body with energy are carbohydrate, fat and protein. These can be obtained by eating foods from the five food groups.'));

		//2
		$hi->addPoint(new Point($hi, 'wofd_rcp_title1', 'Carbohydrate'));
		$hi->addPoint(new Point($hi, 'wofd_rcp_content1', '<p>The fuel for our body, is used during exercise, (in the form of glucose) which is stored in muscle as glycogen. As you exercise, your muscles use the stored glycogen. Muscle can usually store enough glycogen for about 60-90 minutes of high intensity exercise, and these stores need to be replaced between exercise sessions by eating foods high in carbohydrate. Inadequate carbohydrate intake can lead to muscle fatigue which can affect performance.</p><p>Meals should be based on food rich in carbohydrates such as cereals, fruits, vegetables and legumes. Complex carbohydrates such as pasta and bread should be eaten in small quantities. Yogurt also provides carbohydrate in the form of the milk sugar, lactose.</p>'));
		$hi->addPoint(new Point($hi, 'wofd_rcp_bg1', 'http://chefworkout.dev/templates/fleava_front/img/hero1.jpg'));

		//3
		$hi->addPoint(new Point($hi, 'wofd_rcp_title2', 'Protein'));
		$hi->addPoint(new Point($hi, 'wofd_rcp_content2', 'Protein helps repair and rebuild muscle after exercise and can also be used during exercise as an energy source, particularly when carbohydrate reserves are very low. Protein needs of most athletes can be met by a well-balanced diet. You should consume a wide variety of high-quality protein foods such as chicken, turkey, beef, lamb, pork, fish, eggs, dairy foods, nuts and seeds. Some athletes, such as strength trained or endurance athletes often need more protein, with requirements of 1.2-1.6g per kilogram of body mass per day. Such intakes can generally be achieved by the overall increased food intake required to fuel training. Protein supplements and shakes can be very expensive and are not usually necessary. You can make a high-protein milk drink at home at a fraction of the cost by adding skim milk powder to your normal milk drink. Skim milk powder can also be added to other meals such as soup or cereal to further boost protein intake.'));
		$hi->addPoint(new Point($hi, 'wofd_rcp_bg2', 'http://chefworkout.dev/templates/fleava_front/img/hero2.jpg'));

		//4
		$hi->addPoint(new Point($hi, 'wofd_rcp_title3', 'Fat'));
		$hi->addPoint(new Point($hi, 'wofd_rcp_content3', '<p>Fat provides the main fuel source for long duration, low to moderate intensity exercise such as marathons. Even during high intensity exercise, where carbohydrates are the main fuel source, fat is needed to help access those stored carbohydrate (glycogen).</p><p>You should include moderate amounts of ‘healthy’ fats into your daily diet, such as nuts, seeds, fish, reduced-fat dairy foods, lean meat and avocados. Foods high in ‘unhealthy’ fat and low in other nutrients such as biscuits, pastries, chips and deep fried foods should be limited. It is generally not advised to eat foods high in fat immediately before or during intense exercise as fat is slow to digest and can remain in the stomach for a long time.</p>'));
		$hi->addPoint(new Point($hi, 'wofd_rcp_bg3', 'http://chefworkout.dev/templates/fleava_front/img/hero3.jpg'));

		//5
		$hi->addPoint(new Point($hi, 'wofd_mdl_title', 'HYDRATION'));
		$hi->addPoint(new Point($hi, 'wofd_mdl_subtitle', 'Good hydration is one of the most important nutrition priorities for athletes. During exercise your body produces sweat to help cool it down. Athletes who train for long intervals or in hot conditions can lose large amounts of fluid through sweat, which can lead to dehydration.'));
		$hi->addPoint(new Point($hi, 'wofd_mdl_content1', 'Even small amounts of fluid loss can significantly impair performance. It is essential that you drink fluid before, during and after exercise to replace fluid lost from sweating. Keep in mind that thirst is not a good indication of fluid loss. By the time you feel thirsty your body is already dehydrated.'));
		$hi->addPoint(new Point($hi, 'wofd_mdl_content2', 'For low intensity exercise lasting for a short duration, water is very good for rehydration. Water is cheap and convenient and sufficient for most recreational exercisers. For high intensity and endurance sports lasting longer than 60 minutes, a drink which contains carbohydrate and electrolytes, such as milk or a commercial sports drink, is generally more effective than water in enhancing performance. These drinks contain carbohydrate to help delay fatigue by providing glucose to the muscles, and electrolytes to replace sodium lost in sweat.'));
		$hi->addPoint(new Point($hi, 'wofd_mdl_link', 'What is the best drink for sport?'));

		//6
		$hi->addPoint(new Point($hi, 'wofd_low_title', 'Important micronutrients<br> for athletes'));
		$hi->addPoint(new Point($hi, 'wofd_low_content_title1', 'Iron'));
		$hi->addPoint(new Point($hi, 'wofd_low_content1', 'Iron transports oxygen to all parts of the body, including muscles, and helps release energy from cells. If iron levels are low, you can feel tired and low in energy. Iron deficiency is a common problem for athletes, particularly women, vegetarians and adolescents. Hard training stimulates an increase in red blood cell production, increasing the need for iron. Iron can also be lost through damage to red blood cells in the feet due to running on hard surfaces with poor quality shoes, through blood loss from injury and through sweat.'));
		$hi->addPoint(new Point($hi, 'wofd_low_content_title2', 'Calcium'));
		$hi->addPoint(new Point($hi, 'wofd_low_content2', 'Adequate calcium consumption is necessary to develop and maintain strong bones that are resistant to fracture and osteoporosis in later life. Whilst most athletes will have above average bone mass, some female athletes are at high risk of developing osteoporosis prematurely. Loss of periods (known as amenorrhea) due to hard training and low body fat levels means that the body produces less estrogen, which stops bones from reaching peak mass and strength.<br><br>Most athletes need three daily servings of dairy foods to help ensure they get enough calcium. A serving of dairy could include one glass (250mL) of milk, one tub (200g) of yogurt or two slices (40g) of cheese. Teenage athletes should aim for four servings to meet their increased recommended daily intake of calcium.'));
		$hi->addPoint(new Point($hi, 'wofd_low_bg', 'http://chefworkout.dev/templates/fleava_front/img/hero11.jpg'));

		//7
		$hi->addPoint(new Point($hi, 'wofd_ug_title', 'DAIRY’S ROLE IN SPORTS NUTRITION'));
		$hi->addPoint(new Point($hi, 'wofd_ug_subtitle', 'Dairy does more than build strong bones! Dairy is ideal for athletes and recreational exercisers, who want to build lean muscle, speed up recovery and rehydrate effectively.'));
		$hi->addPoint(new Point($hi, 'wofd_ug_content_title1', 'Milk and rehydration'));
		$hi->addPoint(new Point($hi, 'wofd_ug_content1', 'There is increasing interest in the use of milk as a rehydration drink. Milk naturally contains water, carbohydrate and electrolytes. A recent study found that drinking milk after exercise may promote rehydration more effectively than water or sports drinks. Researchers have said, it was likely that the naturally high electrolyte content of milk helped restore the body’s fluid balance after exercise.'));
		$hi->addPoint(new Point($hi, 'wofd_ug_content_title2', 'Dairy helps build lean<br> muscle mass'));
		$hi->addPoint(new Point($hi, 'wofd_ug_content2', 'Studies have shown that the protein from dairy food can help build and maintain muscle. Milk contains about 3.5% protein made up of casein (80%) and whey (20%). The whey protein has a high concentration of the branched chain amino acid – leucine. Leucine has been shown to specifically stimulate building of new muscle protein and dairy protein has been shown to directly stimulate muscle building.'));
		$hi->addPoint(new Point($hi, 'wofd_ug_content_title3', 'Milk speeds up recovery'));
		$hi->addPoint(new Point($hi, 'wofd_ug_content3', 'Dairy products such as milk and yogurt are useful foods for post-exercise recovery because they contain carbohydrate and protein. Studies have shown that chocolate milk may be as good, or better, than sports drinks at helping athletes recover from strenuous exercise.'));

		//8
		$hi->addPoint(new Point($hi, 'wofd_lgm_bg', 'http://chefworkout.dev/templates/fleava_front/img/workout5.jpg'));
		$hi->addPoint(new Point($hi, 'wofd_lgm_title', 'Dairy snacks for sports'));
		$hi->addPoint(new Point($hi, 'wofd_lgm_content1', 'Drinks like plain or flavored milk, smoothies and milkshakes are ideal snack options after exercise. They can also be a cheaper, yet equally effective and convenient alternative to the expensive sports supplements available.'));
		$hi->addPoint(new Point($hi, 'wofd_lgm_content2', 'Other examples of post-exercise recovery snacks containing dairy include:<br>• Drinking yogurt<br>• A tub of flavored yogurt<br>• A cheese and salad sandwich<br>• Ricotta, honey and banana on toast<br>• Low-fat cheesy muffins'));

		$this->em->persist($hi);
		$this->em->flush();
	}

	public function c() {
		$hi = new Page('chef');

		//1
		$hi->addPoint(new Point($hi, 'chef_bg', ''));
		$hi->addPoint(new Point($hi, 'chef_title', 'The Chef'));
		$hi->addPoint(new Point($hi, 'chef_name', 'Nicola Russo'));

		//2
		$hi->addPoint(new Point($hi, 'chef_content1', 'I’m a huge believer in research and development, for me this is one of the secret to improve and be successful.'));
		$hi->addPoint(new Point($hi, 'chef_content2', 'Working in a kitchen of a fine dining restaurant or a luxury hotel means sacrifice and great devotion, it’s a very hard job, constantly under pressure, working long hours.  I spend most of my time in a kitchen together with my culinary team, for this reason I believe is very important build a strong relationship with them, from stewards to my direct Sous chefs I have to make sure they trust me and respect me. This is probably the most difficult part of been a leader.'));

		//3
		$hi->addPoint(new Point($hi, 'chef_mdl_bg', ''));
		$hi->addPoint(new Point($hi, 'chef_mdl_content1', '<p class="text-white lead">I’ve been working with many chefs in my life, people from different nationalities and with different cooking skills.  Germans very picky and crazy on setting standards, French very careful to small details or Japanese working like machines 16 hours in a day. All these people and the places where I worked teach me something good or bad. I think is the choice of a chef to decide what to pick up from each experience and put in your own chef baggage. With all this big amount of knowledge I create a mixture in myself and made my own style, my own chef personality.</p><p class="text-white lead">When you have strong people around you, people will push you to grow , you will be challenged . At the beginning you will have a difficult life and you will fail 1, 2 maybe 3 times  but at the end if you have devotion , passion and positive energy you can achieve anything. </p>'));

		//4
		$hi->addPoint(new Point($hi, 'chef_gray_content1', 'When you are a Chef de Cuisine you obviously need to trust on your kitchen team and you becoming like a coach of a football team. You train , guide and motivate the people working with you.'));
		$hi->addPoint(new Point($hi, 'chef_gray_content2', '<p class="lead">About the culinary side, I’m good on innovations, I love experiment, and try new recipes or cooking techniques. Making a new dish for me is like composing a song you need to find the perfect balance and harmony between 2 or more ingredients. I believe in the of the cuisine of the territory, seasonality of the ingredients  with great respect from the tradition of the Italian cuisine. Where ever I worked I like to mix the products and ingredient.</p><p class="lead">I believe in the of the cuisine of the territory, seasonality of the ingredients  with great respect from the tradition of the Italian cuisine. Where ever I worked I like to mix the products and ingredient.</p>'));

		$this->em->persist($hi);
		$this->em->flush();
	}

	public function d() {
		$hi = new Page('cuisine');

		//1
		$hi->addPoint(new Point($hi, 'cuisine_bg', ''));
		$hi->addPoint(new Point($hi, 'cuisine_title1', 'Just some headings'));
		$hi->addPoint(new Point($hi, 'cuisine_content1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'));

		$hi->addPoint(new Point($hi, 'cuisine_title2', 'And yet another heading'));
		$hi->addPoint(new Point($hi, 'cuisine_content2', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident'));

		$gr = new Page('gallery');

		//2
		$gr->addPoint(new Point($gr, 'gallery_title', 'FOOD GALLERY'));
		$gr->addPoint(new Point($gr, 'gallery_subtitle', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab.'));

		$this->em->persist($hi);
		$this->em->persist($gr);
		$this->em->flush();
	}

	public function e() {
		$hi = new Page('connect');

		//1
		$hi->addPoint(new Point($hi, 'connect_bg', ''));
		$hi->addPoint(new Point($hi, 'connect_title', ''));

		$this->em->persist($hi);
		$this->em->flush();
	}

	public function f() {
		$hi = new Page('journal');

		//1
		$hi->addPoint(new Point($hi, 'journal_bg', 'http://chefworkout.dev/templates/fleava_front/img/hero7.jpg'));
		$hi->addPoint(new Point($hi, 'journal_main_title', 'JOURNALS'));
		$hi->addPoint(new Point($hi, 'journal_main_subtitle', 'A collection of high quality modern <br> recipes from the chef.'));

		$this->em->persist($hi);
		$this->em->flush();
	}

	public function g() {
		$hi = new Page('recipe');

		//1
		$hi->addPoint(new Point($hi, 'recipe_bg', 'http://chefworkout.dev/templates/fleava_front/img/hero7.jpg'));
		$hi->addPoint(new Point($hi, 'recipe_main_title', 'CHEF RECIPES'));
		$hi->addPoint(new Point($hi, 'recipe_main_subtitle', 'A collection of high quality modern <br> recipes from the chef.'));

		$this->em->persist($hi);
		$this->em->flush();
	}

	public function h() {
		$hi = new Page('healthy');

		//1
		$hi->addPoint(new Point($hi, 'healthy_bg', 'http://chefworkout.dev/templates/fleava_front/img/hero7.jpg'));
		$hi->addPoint(new Point($hi, 'healthy_main_title', 'HEALTHY RECIPES'));
		$hi->addPoint(new Point($hi, 'healthy_main_subtitle', 'A collection of high quality modern <br> recipes from the chef.'));

		$this->em->persist($hi);
		$this->em->flush();
	}
} 