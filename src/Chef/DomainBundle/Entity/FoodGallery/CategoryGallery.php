<?php

namespace Chef\DomainBundle\Entity\FoodGallery;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class CategoryGallery extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $name;

	/** @ORM\Column(type="string") */
	private $alias;

	public function __construct($name, $alias) {
		parent::__construct();

		$this->name = $name;

		$this->alias = $alias;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getAlias() {
		return $this->alias;
	}

	public function setAlias($alias) {
		$this->alias = $alias;
	}

} 