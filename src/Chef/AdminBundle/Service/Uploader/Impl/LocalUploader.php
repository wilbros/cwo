<?php

namespace Chef\AdminBundle\Service\Uploader\Impl;

use Chef\AdminBundle\Service\Uploader\Uploader;
use JMS\DiExtraBundle\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LocalUploader
 *
 * @JMS\Service("uploader.local")
 */
class LocalUploader implements Uploader {
    private $uploadPath;
    private $uploadUrl;
	private $folder;

    /**
     * @JMS\InjectParams({
     *      "uploadPath" = @JMS\Inject("%upload.local.path%"),
     *      "uploadUrl" = @JMS\Inject("%upload.local.url%")
     * })
     */
    public function __construct($uploadPath, $uploadUrl) {
        $this->uploadPath = $uploadPath;
        $this->uploadUrl = $uploadUrl;
    }

    /** {@inheritDoc} */
    public function getUploadPath() {
        return $this->uploadPath;
    }

    /** {@inheritDoc} */
    public function upload(UploadedFile $file, $filename = "") {
        $now = new \DateTime('NOW');
        $dir = sprintf("%s/%s/%s",
            $now->format('Y'),
            $now->format('m'),
            $now->format('d')
        );

	    if($this->folder != null || strlen(trim($this->folder)) > 0) {
			$dir = sprintf("%s/%s", $this->folder, $dir);
	    }

        if(!file_exists($this->uploadPath.'/'.$dir)) {
            mkdir($this->uploadPath.'/'.$dir, 0777, true);
        }

        $newFilename = uniqid(pathinfo(str_replace(' ', '_', $file->getClientOriginalName()), PATHINFO_FILENAME)."_").'.'.$file->getClientOriginalExtension();
        $file->move($this->uploadPath.'/'.$dir, $newFilename);

        return sprintf("%s/%s", $this->uploadUrl, $dir.'/'.$newFilename);
    }

    /** {@inheritDoc} */
    public function remove($filename) {
        unlink($this->uploadPath.'/'.$filename);
    }

	public function getFolder() {
		return $this->folder;
	}

	public function setFolder($folder) {
		$this->folder = $folder;

		return $this;
	}
}
