<?php

namespace Chef\DomainBundle\Entity\HealthyRecipe;

use Chef\DomainBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class RecipeMenuHealthy
 * @package Chef\DomainBundle\Entity\HealthyRecipe
 * @ORM\Entity
 */
class RecipeMenuHealthy extends AbstractEntity {

	/** @ORM\Column(type="string") */
	private $name;

	public function __construct($name) {
		parent::__construct();
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

} 